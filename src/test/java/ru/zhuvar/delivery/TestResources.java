package ru.zhuvar.delivery;

public class TestResources {

    public static final String ORDER_CREATED_JSON = "/core/domain/order/order_created.json";
    public static final String ORDER_ASSIGNED_JSON = "/core/domain/order/order_assigned.json";
    public static final String COURIER_READY_JSON = "/core/domain/courier/courier_ready.json";
    public static final String COURIER_READY_ON_CAR_JSON = "/core/domain/courier/courier_ready_on_car.json";
    public static final String COURIER_BUSY_JSON = "/core/domain/courier/courier_busy.json";
    public static final String COURIER_BUSY_ON_CAR_WITH_ORDER_JSON = "/core/domain/courier/courier_busy_on_car_with_order.json";
    public static final String ORDER_ASSIGNED_NEAR_START_LOCATION_JSON = "/core/domain/order/order_assigned_near_start_location.json";

    public static final String CREATED_OR_ASSIGNED_ORDERS_JSON = "/core/application/queries/getcreatedorassignedorders/created_or_assigned_orders.json";
    public static final String READY_OR_BUSY_COURIERS_JSON = "/core/application/queries/getreadyorbusycouriers/ready_or_busy_couriers.json";
    public static final String CREATED_OR_ASSIGNED_ORDER_JSON = "/core/application/queries/getcreatedorassignedorders/created_or_assigned_order.json";
    public static final String READY_OR_BUSY_COURIER_JSON = "/core/application/queries/getreadyorbusycouriers/ready_or_busy_courier.json";

    public static final String OUTBOX_MESSAGE_ASSIGNED_JSON = "/utils/extension/outbox/outbox_message_assigned.json";
    public static final String OUTBOX_MESSAGE_ASSIGNED_SENT_JSON = "/utils/extension/outbox/outbox_message_assigned_sent.json";
    public static final String OUTBOX_MESSAGE_ASSIGNED_WAITING_JSON = "/utils/extension/outbox/outbox_message_assigned_waiting.json";

    public static final String TRUNCATE_ORDERS_SCRIPT = "/infrastructure/adapters/postgres/order/truncate_orders.sql";
    public static final String INSERT_CREATED_ORDER_SCRIPT = "/infrastructure/adapters/postgres/order/insert_created_order.sql";
    public static final String INSERT_ASSIGNED_ORDER_SCRIPT = "/infrastructure/adapters/postgres/order/insert_assigned_order.sql";

    public static final String TRUNCATE_COURIERS_SCRIPT = "/infrastructure/adapters/postgres/courier/truncate_couriers.sql";
    public static final String INSERT_READY_COURIER_SCRIPT = "/infrastructure/adapters/postgres/courier/insert_ready_courier.sql";
    public static final String INSERT_BUSY_COURIER_SCRIPT = "/infrastructure/adapters/postgres/courier/insert_busy_courier.sql";

    public static final String TRUNCATE_OUTBOX_MESSAGES_SCRIPT = "/infrastructure/adapters/postgres/outbox/truncate_outbox_messages.sql";
    public static final String INSERT_OUTBOX_MESSAGE_ASSIGNED_SCRIPT = "/infrastructure/adapters/postgres/outbox/insert_message_assigned.sql";
    public static final String INSERT_OUTBOX_MESSAGE_ASSIGNED_POSTED_SCRIPT = "/infrastructure/adapters/postgres/outbox/insert_message_assigned_posted.sql";
    public static final String INSERT_OUTBOX_MESSAGE_ASSIGNED_WAITING_SCRIPT = "/infrastructure/adapters/postgres/outbox/insert_message_assigned_waiting.sql";

}
