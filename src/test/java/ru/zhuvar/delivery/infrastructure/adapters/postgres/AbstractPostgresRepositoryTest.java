package ru.zhuvar.delivery.infrastructure.adapters.postgres;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.zhuvar.delivery.api.adapters.kafka.basket.BasketConfirmedIntegrationEventListenerListener;
import ru.zhuvar.delivery.api.adapters.scheduler.job.PostOutboxMessagesJob;
import ru.zhuvar.delivery.config.kafka.KafkaConsumerConfig;
import ru.zhuvar.delivery.config.kafka.KafkaProducerConfig;
import ru.zhuvar.delivery.config.scheduler.SchedulerConfiguration;
import ru.zhuvar.delivery.config.scheduler.config.AssignOrderJobConfig;
import ru.zhuvar.delivery.config.scheduler.config.MoveCouriersJobConfig;
import ru.zhuvar.delivery.config.scheduler.config.PostOutboxMessagesJobConfig;
import ru.zhuvar.delivery.infrastructure.adapters.kafka.notification.integrationevents.OrderStatusChangedIntegrationEvent;

@SpringBootTest
@DirtiesContext
@Testcontainers
public abstract class AbstractPostgresRepositoryTest {

    @MockBean
    public AssignOrderJobConfig assignOrderJobConfig;

    @MockBean
    public MoveCouriersJobConfig moveCouriersJobConfig;

    @MockBean
    public PostOutboxMessagesJobConfig postOutboxMessagesJobConfig;

    @MockBean
    public PostOutboxMessagesJob postOutboxMessagesJob;

    @MockBean
    public SchedulerConfiguration schedulerConfiguration;

    @MockBean
    public KafkaConsumerConfig kafkaListenerConfig;

    @MockBean
    public BasketConfirmedIntegrationEventListenerListener basketConfirmedIntegrationEventListenerListener;

    @MockBean
    public KafkaProducerConfig kafkaProducerConfig;

    @MockBean
    public KafkaTemplate<String, OrderStatusChangedIntegrationEvent> orderStatusChangedIntegrationEventKafkaTemplate;

    private static final String DB_NAME = "delivery";

    @Container
    @ServiceConnection
    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:15-alpine").withDatabaseName(DB_NAME);

    static {
        postgres.start();
    }
}
