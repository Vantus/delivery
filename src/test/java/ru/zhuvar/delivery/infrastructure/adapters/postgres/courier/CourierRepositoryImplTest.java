package ru.zhuvar.delivery.infrastructure.adapters.postgres.courier;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.core.ports.CourierRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.AbstractPostgresRepositoryTest;

import java.util.List;
import java.util.UUID;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.TestResources.COURIER_READY_JSON;
import static ru.zhuvar.delivery.TestResources.INSERT_READY_COURIER_SCRIPT;
import static ru.zhuvar.delivery.TestResources.TRUNCATE_COURIERS_SCRIPT;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

public class CourierRepositoryImplTest extends AbstractPostgresRepositoryTest {

    @Autowired
    private CourierRepository repository;

    private static final UUID READY_COURIER_ID = UUID.fromString("f8c3de3d-1fea-4d7c-a8b0-29f63c4c3454");

    @Test
    @Sql(scripts = TRUNCATE_COURIERS_SCRIPT)
    void add() {
        // given
        Courier courier = parseFile(COURIER_READY_JSON, Courier.class);
        // when
        Courier saved = repository.add(courier);
        // then
        assertEquals(courier, saved);
    }

    @Test
    @Sql(scripts = {
            TRUNCATE_COURIERS_SCRIPT,
            INSERT_READY_COURIER_SCRIPT
    })
    void update() {
        // given
        Courier courier = parseFile(COURIER_READY_JSON, Courier.class);
        courier.endWork();
        // when then
        assertDoesNotThrow(() -> repository.update(courier));
    }

    @Test
    @Sql(scripts = {
            TRUNCATE_COURIERS_SCRIPT,
            INSERT_READY_COURIER_SCRIPT
    })
    void getById() {
        // when
        Courier presentedCourier = repository.getById(READY_COURIER_ID);
        // then
        Courier courierExpected = parseFile(COURIER_READY_JSON, Courier.class);
        assertEquals(courierExpected, presentedCourier);
    }

    @Test
    @Sql(scripts = {
            TRUNCATE_COURIERS_SCRIPT,
            INSERT_READY_COURIER_SCRIPT
    })
    void getAllReady() {
        // when
        List<Courier> readyCouriers = repository.getAllReady();
        // then
        Courier courierExpected = parseFile(COURIER_READY_JSON, Courier.class);
        assertEquals(singletonList(courierExpected), readyCouriers);
    }
}
