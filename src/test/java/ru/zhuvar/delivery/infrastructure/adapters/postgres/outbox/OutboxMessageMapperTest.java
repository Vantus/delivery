package ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.entity.OutboxMessageEntity;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.mapper.OutboxMessageMapper;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.mapper.OutboxMessageMapperImpl;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.TestResources.OUTBOX_MESSAGE_ASSIGNED_JSON;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

@SpringBootTest(classes = {OutboxMessageMapperImpl.class})
public class OutboxMessageMapperTest {

    @Autowired
    public OutboxMessageMapper mapper;

    @Test
    void toEntity_and_fromEntity() {
        // given
        OutboxMessage outboxMessage = parseFile(OUTBOX_MESSAGE_ASSIGNED_JSON, OutboxMessage.class);

        // when toEntity
        OutboxMessageEntity entity = mapper.toEntity(outboxMessage);
        // then toEntity
        assertEquals(outboxMessage.getId(), entity.getId());
        assertEquals(outboxMessage.getType(), entity.getType());
        assertEquals(outboxMessage.getMessage(), entity.getMessage());

        // when fromEntity
        OutboxMessage model = mapper.fromEntity(entity);
        // then fromEntity
        assertEquals(outboxMessage.getId(), model.getId());
        assertEquals(outboxMessage.getType(), model.getType());
        assertEquals(outboxMessage.getMessage(), model.getMessage());
    }
}
