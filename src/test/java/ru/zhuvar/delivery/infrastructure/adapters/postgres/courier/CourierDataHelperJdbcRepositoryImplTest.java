package ru.zhuvar.delivery.infrastructure.adapters.postgres.courier;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers.ReadyOrBusyCourier;
import ru.zhuvar.delivery.core.ports.CourierDataHelperRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.AbstractPostgresRepositoryTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.TestResources.INSERT_BUSY_COURIER_SCRIPT;
import static ru.zhuvar.delivery.TestResources.INSERT_READY_COURIER_SCRIPT;
import static ru.zhuvar.delivery.TestResources.READY_OR_BUSY_COURIERS_JSON;
import static ru.zhuvar.delivery.TestResources.TRUNCATE_COURIERS_SCRIPT;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

public class CourierDataHelperJdbcRepositoryImplTest extends AbstractPostgresRepositoryTest {

    @Autowired
    private CourierDataHelperRepository repository;

    @Test
    @Sql(scripts = {
            TRUNCATE_COURIERS_SCRIPT,
            INSERT_READY_COURIER_SCRIPT,
            INSERT_BUSY_COURIER_SCRIPT
    })
    void getAllReadyAndBusyCouriers() {
        // given
        List<ReadyOrBusyCourier> expected = parseFile(READY_OR_BUSY_COURIERS_JSON, new TypeReference<>() {
        });
        // when
        List<ReadyOrBusyCourier> allReadyOrBusyCouriers = repository.getAllReadyAndBusyCouriers();
        // then
        assertEquals(expected, allReadyOrBusyCouriers);
    }
}
