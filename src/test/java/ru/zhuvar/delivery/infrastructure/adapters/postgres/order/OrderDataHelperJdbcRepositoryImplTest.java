package ru.zhuvar.delivery.infrastructure.adapters.postgres.order;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders.CreatedOrAssignedOrder;
import ru.zhuvar.delivery.core.ports.OrderDataHelperRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.AbstractPostgresRepositoryTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.TestResources.CREATED_OR_ASSIGNED_ORDERS_JSON;
import static ru.zhuvar.delivery.TestResources.INSERT_ASSIGNED_ORDER_SCRIPT;
import static ru.zhuvar.delivery.TestResources.INSERT_CREATED_ORDER_SCRIPT;
import static ru.zhuvar.delivery.TestResources.TRUNCATE_ORDERS_SCRIPT;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

public class OrderDataHelperJdbcRepositoryImplTest extends AbstractPostgresRepositoryTest {

    @Autowired
    private OrderDataHelperRepository repository;

    @Test
    @Sql(scripts = {
            TRUNCATE_ORDERS_SCRIPT,
            INSERT_CREATED_ORDER_SCRIPT,
            INSERT_ASSIGNED_ORDER_SCRIPT
    })
    void getAllCreatedAndAssignedOrders() {
        // when
        List<CreatedOrAssignedOrder> allCreatedOrAssignedOrders = repository.getAllCreatedAndAssignedOrders();
        // then
        List<CreatedOrAssignedOrder> expected = parseFile(CREATED_OR_ASSIGNED_ORDERS_JSON, new TypeReference<>() {
        });
        assertEquals(expected, allCreatedOrAssignedOrders);
    }
}
