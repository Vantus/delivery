package ru.zhuvar.delivery.infrastructure.adapters.postgres.courier;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.entity.CourierEntity;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.mapper.CourierMapper;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.mapper.CourierMapperImpl;
import ru.zhuvar.delivery.utils.JsonUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.TestResources.COURIER_READY_JSON;


/**
 * Тест представлен для поддержки процесса преобразования агрегата {@code Courier} в БД-сущность
 */
@SpringBootTest(classes = {CourierMapperImpl.class})
public class CourierMapperTest {

    @Autowired
    public CourierMapper mapper;

    @Test
    void toEntity_and_toAggregate() {
        // given
        Courier courier = JsonUtils.parseFile(COURIER_READY_JSON, Courier.class);

        // when toEntity
        CourierEntity courierEntity = mapper.toEntity(courier);
        // then toEntity
        assertEquals(courier.getId(), courierEntity.getId());
        assertEquals(courier.getName(), courierEntity.getName());
        assertEquals(courier.getTransport(), courierEntity.getTransport());
        assertEquals(courier.getLocation(), courierEntity.getLocation());

        // when toAggregate
        Courier aggregate = mapper.toAggregate(courierEntity);
        //then toAggregate
        assertEquals(courier.getId(), aggregate.getId());
        assertEquals(courier.getName(), aggregate.getName());
        assertEquals(courier.getTransport(), aggregate.getTransport());
        assertEquals(courier.getLocation(), aggregate.getLocation());
    }
}
