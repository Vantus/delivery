package ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.zhuvar.delivery.core.application.domain.event.handlers.DomainEventsResolver;
import ru.zhuvar.delivery.core.domain.order.domainEvents.OrderStatusChangedDomainEvent;
import ru.zhuvar.delivery.core.ports.OutboxMessageRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.service.OutboxMessagesService;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessage;

import java.util.List;
import java.util.UUID;

import static java.util.UUID.fromString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.zhuvar.delivery.TestResources.OUTBOX_MESSAGE_ASSIGNED_JSON;
import static ru.zhuvar.delivery.core.domain.order.OrderStatus.ASSIGNED;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

@SpringBootTest(classes = {OutboxMessagesService.class})
public class OutboxMessagesServiceTest {

    @Autowired
    private OutboxMessagesService outboxMessagesService;

    @MockBean
    private OutboxMessageRepository outboxMessageRepository;

    @MockBean
    private DomainEventsResolver domainEventsResolver;

    private static final Integer POLL_BATCH_SIZE = 5;
    private static final UUID OUTBOX_MESSAGE_ASSIGNED_ID = fromString("2f983a59-b796-4be2-848b-c9ce1cab0dfe");

    @Test
    void post() {
        // given
        OutboxMessage outboxMessage = parseFile(OUTBOX_MESSAGE_ASSIGNED_JSON, OutboxMessage.class);

        when(outboxMessageRepository.getUnposted(POLL_BATCH_SIZE)).thenReturn(List.of(outboxMessage));
        // when
        outboxMessagesService.post(POLL_BATCH_SIZE);
        // then
        verify(domainEventsResolver).resolve(argThat(domainEvent -> {
            assertInstanceOf(OrderStatusChangedDomainEvent.class, domainEvent);
            OrderStatusChangedDomainEvent castedDomainEvent = (OrderStatusChangedDomainEvent) domainEvent;
            assertEquals(OUTBOX_MESSAGE_ASSIGNED_ID, castedDomainEvent.getId());
            assertEquals(ASSIGNED, castedDomainEvent.getOrderStatus());
            return true;
        }));
        verify(outboxMessageRepository).updateAll(argThat(unposted -> {
            assertFalse(unposted.isEmpty());
            OutboxMessage first = unposted.getFirst();
            return true;
        }));
    }


}
