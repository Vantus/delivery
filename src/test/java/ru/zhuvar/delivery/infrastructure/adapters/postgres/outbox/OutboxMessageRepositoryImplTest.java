package ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.zhuvar.delivery.core.ports.OutboxMessageRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.AbstractPostgresRepositoryTest;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessage;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.TestResources.INSERT_OUTBOX_MESSAGE_ASSIGNED_POSTED_SCRIPT;
import static ru.zhuvar.delivery.TestResources.INSERT_OUTBOX_MESSAGE_ASSIGNED_SCRIPT;
import static ru.zhuvar.delivery.TestResources.INSERT_OUTBOX_MESSAGE_ASSIGNED_WAITING_SCRIPT;
import static ru.zhuvar.delivery.TestResources.OUTBOX_MESSAGE_ASSIGNED_JSON;
import static ru.zhuvar.delivery.TestResources.OUTBOX_MESSAGE_ASSIGNED_WAITING_JSON;
import static ru.zhuvar.delivery.TestResources.TRUNCATE_OUTBOX_MESSAGES_SCRIPT;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

public class OutboxMessageRepositoryImplTest extends AbstractPostgresRepositoryTest {

    @Autowired
    private OutboxMessageRepository repository;

    @Test
    @Sql(scripts = TRUNCATE_OUTBOX_MESSAGES_SCRIPT)
    void add() {
        // given
        OutboxMessage outboxMessage = parseFile(OUTBOX_MESSAGE_ASSIGNED_JSON, OutboxMessage.class);
        // when
        OutboxMessage saved = repository.add(outboxMessage);
        // then
        assertEquals(outboxMessage, saved);
    }

    @Test
    @Sql(scripts = {
            TRUNCATE_OUTBOX_MESSAGES_SCRIPT,
            INSERT_OUTBOX_MESSAGE_ASSIGNED_WAITING_SCRIPT
    })
    void updateAll() {
        // given
        OutboxMessage outboxMessage = parseFile(OUTBOX_MESSAGE_ASSIGNED_WAITING_JSON, OutboxMessage.class);
        // when then
        assertDoesNotThrow(() -> repository.updateAll(singletonList(outboxMessage)));
    }

    @Test
    @Sql(scripts = {
            TRUNCATE_OUTBOX_MESSAGES_SCRIPT,
            INSERT_OUTBOX_MESSAGE_ASSIGNED_SCRIPT,
            INSERT_OUTBOX_MESSAGE_ASSIGNED_POSTED_SCRIPT
    })
    void getUnposted() {
        // when
        List<OutboxMessage> unposted = repository.getUnposted(10);
        // then
        OutboxMessage outboxMessage = parseFile(OUTBOX_MESSAGE_ASSIGNED_JSON, OutboxMessage.class);
        assertEquals(singletonList(outboxMessage), unposted);
    }
}
