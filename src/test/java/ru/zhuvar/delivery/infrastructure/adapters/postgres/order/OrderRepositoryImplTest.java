package ru.zhuvar.delivery.infrastructure.adapters.postgres.order;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.ports.OrderRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.AbstractPostgresRepositoryTest;

import java.util.List;
import java.util.UUID;

import static java.util.Collections.singletonList;
import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.TestResources.INSERT_ASSIGNED_ORDER_SCRIPT;
import static ru.zhuvar.delivery.TestResources.INSERT_CREATED_ORDER_SCRIPT;
import static ru.zhuvar.delivery.TestResources.ORDER_ASSIGNED_JSON;
import static ru.zhuvar.delivery.TestResources.ORDER_CREATED_JSON;
import static ru.zhuvar.delivery.TestResources.TRUNCATE_ORDERS_SCRIPT;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

public class OrderRepositoryImplTest extends AbstractPostgresRepositoryTest {

    @Autowired
    private OrderRepository repository;

    private static final UUID INSERTED_ORDER_ID = UUID.fromString("e58ed763-928c-4155-bee9-fdbaaadc15f3");

    @Test
    @Sql(scripts = TRUNCATE_ORDERS_SCRIPT)
    void add() {
        // given
        Order order = parseFile(ORDER_CREATED_JSON, Order.class);
        // when
        Order saved = repository.add(order);
        // then
        assertEquals(order, saved);
    }

    @Test
    @Sql(scripts = {
            TRUNCATE_ORDERS_SCRIPT,
            INSERT_CREATED_ORDER_SCRIPT
    })
    void update() {
        // given
        Order order = parseFile(ORDER_CREATED_JSON, Order.class);
        order.assignToCourier(randomUUID());
        // when then
        assertDoesNotThrow(() -> repository.update(order));
    }

    @Test
    @Sql(scripts = {
            TRUNCATE_ORDERS_SCRIPT,
            INSERT_CREATED_ORDER_SCRIPT
    })
    void getById() {
        // when
        Order presentedOrder = repository.getById(INSERTED_ORDER_ID);
        // then
        Order orderExpected = parseFile(ORDER_CREATED_JSON, Order.class);
        assertEquals(orderExpected, presentedOrder);
    }

    @Test
    @Sql(scripts = {
            TRUNCATE_ORDERS_SCRIPT,
            INSERT_CREATED_ORDER_SCRIPT
    })
    void getAllCreated() {
        // when
        List<Order> createdOrders = repository.getAllCreated();
        // then
        Order orderExpected = parseFile(ORDER_CREATED_JSON, Order.class);
        assertEquals(singletonList(orderExpected), createdOrders);
    }


    @Test
    @Sql(scripts = {
            TRUNCATE_ORDERS_SCRIPT,
            INSERT_ASSIGNED_ORDER_SCRIPT
    })
    void getAllAssigned() {
        // when
        List<Order> createdOrders = repository.getAllAssigned();
        // then
        Order orderExpected = parseFile(ORDER_ASSIGNED_JSON, Order.class);
        assertEquals(singletonList(orderExpected), createdOrders);
    }
}
