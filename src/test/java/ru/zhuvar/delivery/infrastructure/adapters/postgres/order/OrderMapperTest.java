package ru.zhuvar.delivery.infrastructure.adapters.postgres.order;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.order.entity.OrderEntity;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.order.mapper.OrderMapper;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.order.mapper.OrderMapperImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.TestResources.ORDER_CREATED_JSON;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

/**
 * Тест представлен для поддержки процесса преобразования агрегата {@code Order} в БД-сущность
 */
@SpringBootTest(classes = {OrderMapperImpl.class})
public class OrderMapperTest {

    @Autowired
    public OrderMapper mapper;

    @Test
    void toEntity_and_toAggregate() {
        // given
        Order order = parseFile(ORDER_CREATED_JSON, Order.class);

        // when toEntity
        OrderEntity orderEntity = mapper.toEntity(order);
        // then toEntity
        assertEquals(order.getId(), orderEntity.getId());
        assertEquals(order.getCourierId(), orderEntity.getCourierId());
        assertEquals(order.getLocation(), orderEntity.getLocation());
        assertEquals(order.getWeight(), orderEntity.getWeight());
        assertEquals(order.getStatus(), orderEntity.getStatus());

        // when toAggregate
        Order aggregate = mapper.toAggregate(orderEntity);
        // then toAggregate
        assertEquals(order.getId(), aggregate.getId());
        assertEquals(order.getCourierId(), aggregate.getCourierId());
        assertEquals(order.getLocation(), aggregate.getLocation());
        assertEquals(order.getWeight(), aggregate.getWeight());
        assertEquals(order.getStatus(), aggregate.getStatus());

    }
}
