package ru.zhuvar.delivery.api.adapters.http.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.zhuvar.delivery.TestResources;
import ru.zhuvar.delivery.api.adapters.http.contract.model.Order;
import ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders.CreatedOrAssignedOrder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

@SpringBootTest(classes = OrderDtoMapperImpl.class)
public class OrderDtoMapperTest {

    @Autowired
    private OrderDtoMapper mapper;

    @Test
    void toDto() {
        // given
        CreatedOrAssignedOrder createdOrAssignedOrder = parseFile(TestResources.CREATED_OR_ASSIGNED_ORDER_JSON, CreatedOrAssignedOrder.class);
        // when
        Order dto = mapper.toDto(createdOrAssignedOrder);
        // then
        assertEquals(createdOrAssignedOrder.orderId(), dto.getId());
        assertEquals(createdOrAssignedOrder.location().x(), dto.getLocation().getX());
        assertEquals(createdOrAssignedOrder.location().y(), dto.getLocation().getY());
    }
}
