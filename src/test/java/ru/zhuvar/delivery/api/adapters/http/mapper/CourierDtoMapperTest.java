package ru.zhuvar.delivery.api.adapters.http.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.zhuvar.delivery.TestResources;
import ru.zhuvar.delivery.api.adapters.http.contract.model.Courier;
import ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers.ReadyOrBusyCourier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

@SpringBootTest(classes = CourierDtoMapperImpl.class)
public class CourierDtoMapperTest {

    @Autowired
    private CourierDtoMapperImpl mapper;

    @Test
    void toDto() {
        // given
        ReadyOrBusyCourier courier = parseFile(TestResources.READY_OR_BUSY_COURIER_JSON, ReadyOrBusyCourier.class);
        // when
        Courier dto = mapper.toDto(courier);
        // then
        assertEquals(courier.courierId(), dto.getId());
        assertEquals(courier.name(), dto.getName());
        assertEquals(courier.location().x(), dto.getLocation().getX());
        assertEquals(courier.location().y(), dto.getLocation().getY());
    }
}
