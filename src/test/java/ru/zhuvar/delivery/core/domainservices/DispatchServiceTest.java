package ru.zhuvar.delivery.core.domainservices;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.core.domain.courier.Transport;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;
import ru.zhuvar.delivery.core.domain.sharedkernel.Weight;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(classes = DispatchServiceImpl.class)
public class DispatchServiceTest {

    @Autowired
    private DispatchService dispatchService;

    @Test
    @SneakyThrows
    void dispatchSuccess() {
        // given
        Courier courier = Courier.createCourier("test-name", Transport.PEDESTRIAN);
        Order order = Order.createOrder(UUID.randomUUID(), new Location(1, 1), new Weight(1));
        // when
        Courier dispatchedCourier = dispatchService.dispatch(order, List.of(courier));
        // then
        assertEquals(courier, dispatchedCourier);
    }

    @Test
    void dispatchFailed_whenOnBigWeight() {
        // given
        Courier courier = Courier.createCourier("test-name", Transport.PEDESTRIAN);
        Order order = Order.createOrder(UUID.randomUUID(), new Location(9, 9), new Weight(20));
        // when then
        assertThrows(DispatchException.class, () -> dispatchService.dispatch(order, List.of(courier)));

    }
}
