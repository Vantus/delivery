package ru.zhuvar.delivery.core.application.commands.movecouriers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.ports.CourierRepository;
import ru.zhuvar.delivery.core.ports.OrderRepository;
import ru.zhuvar.delivery.core.ports.UnitOfWork;

import java.util.List;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.zhuvar.delivery.TestResources.COURIER_BUSY_ON_CAR_WITH_ORDER_JSON;
import static ru.zhuvar.delivery.TestResources.ORDER_ASSIGNED_JSON;
import static ru.zhuvar.delivery.TestResources.ORDER_ASSIGNED_NEAR_START_LOCATION_JSON;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

@SpringBootTest(classes = {MoveCouriersHandler.class})
public class MoveCouriersHandlerTest {

    @Autowired
    private MoveCouriersHandler moveCouriersHandler;

    @MockBean
    private CourierRepository courierRepository;

    @MockBean
    private OrderRepository orderRepository;


    @MockBean
    private UnitOfWork unitOfWork;

    @Test
    void apply_whenNoAssignedOrders() {
        // given
        when(orderRepository.getAllAssigned()).thenReturn(emptyList());
        // when
        boolean apply = moveCouriersHandler.apply(new MoveCouriersCommand());
        // then
        assertFalse(apply);
        verify(orderRepository, never()).getById(any());
        verify(unitOfWork, never()).commit();
    }

    @Test
    void apply_successWhenOrderNotFinished() {
        // given
        Order assignedOrder = parseFile(ORDER_ASSIGNED_JSON, Order.class);
        when(orderRepository.getAllAssigned()).thenReturn(List.of(assignedOrder));

        Courier busyCourier = parseFile(COURIER_BUSY_ON_CAR_WITH_ORDER_JSON, Courier.class);
        when(courierRepository.getById(assignedOrder.getCourierId())).thenReturn(busyCourier);
        // when
        boolean apply = moveCouriersHandler.apply(new MoveCouriersCommand());
        // then
        assertTrue(apply);
        verify(unitOfWork, never()).registerModified(assignedOrder);
        verify(unitOfWork).registerModified(busyCourier);
        verify(unitOfWork).commit();
    }

    @Test
    void apply_successWhenOrderFinished() {
        // given
        Order assignedOrder = parseFile(ORDER_ASSIGNED_NEAR_START_LOCATION_JSON, Order.class);
        when(orderRepository.getAllAssigned()).thenReturn(List.of(assignedOrder));

        Courier busyCourier = parseFile(COURIER_BUSY_ON_CAR_WITH_ORDER_JSON, Courier.class);
        when(courierRepository.getById(assignedOrder.getCourierId())).thenReturn(busyCourier);
        // when
        boolean apply = moveCouriersHandler.apply(new MoveCouriersCommand());
        // then
        assertTrue(apply);
        verify(unitOfWork).registerModified(assignedOrder);
        verify(unitOfWork).registerModified(busyCourier);
        verify(unitOfWork).commit();
    }

}
