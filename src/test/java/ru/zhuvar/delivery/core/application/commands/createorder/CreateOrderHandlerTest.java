package ru.zhuvar.delivery.core.application.commands.createorder;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;
import ru.zhuvar.delivery.core.domain.sharedkernel.Weight;
import ru.zhuvar.delivery.core.ports.GeoClient;
import ru.zhuvar.delivery.core.ports.UnitOfWork;

import java.util.UUID;

import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {CreateOrderHandler.class})
public class CreateOrderHandlerTest {

    @Autowired
    private CreateOrderHandler createOrderHandler;

    @MockBean
    private UnitOfWork unitOfWork;

    @MockBean
    private GeoClient geoClient;

    private static final UUID ORDER_ID = randomUUID();
    private static final Location ORDER_LOCATION = new Location(4, 2);
    private static final String ORDER_ADDRESS = "someAddress";
    private static final Weight ORDER_WEIGHT = new Weight(10);

    @Test
    void apply_whenSuccess() {
        // given
        CreateOrderCommand command = new CreateOrderCommand(
                ORDER_ID,
                ORDER_ADDRESS,
                ORDER_WEIGHT);

        Mockito.when(geoClient.getLocationByAddress(ORDER_ADDRESS)).thenReturn(ORDER_LOCATION);
        // when
        boolean apply = createOrderHandler.apply(command);
        // then
        assertTrue(apply);
        verify(unitOfWork).registerNew(argThat(order -> {
            Order orderAggregate = (Order) order;
            assertEquals(ORDER_ID, orderAggregate.getId());
            assertEquals(ORDER_LOCATION, orderAggregate.getLocation());
            assertEquals(0, ORDER_WEIGHT.compareTo(orderAggregate.getWeight()));
            return true;
        }));
        verify(unitOfWork).commit();
    }
}
