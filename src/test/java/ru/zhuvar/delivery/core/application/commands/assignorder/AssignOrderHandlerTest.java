package ru.zhuvar.delivery.core.application.commands.assignorder;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.domainservices.DispatchServiceImpl;
import ru.zhuvar.delivery.core.ports.CourierRepository;
import ru.zhuvar.delivery.core.ports.OrderRepository;
import ru.zhuvar.delivery.core.ports.UnitOfWork;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.zhuvar.delivery.TestResources.COURIER_BUSY_JSON;
import static ru.zhuvar.delivery.TestResources.COURIER_READY_JSON;
import static ru.zhuvar.delivery.TestResources.COURIER_READY_ON_CAR_JSON;
import static ru.zhuvar.delivery.TestResources.ORDER_CREATED_JSON;
import static ru.zhuvar.delivery.utils.JsonUtils.parseFile;

@SpringBootTest(classes = {AssignOrderHandler.class, DispatchServiceImpl.class})
public class AssignOrderHandlerTest {

    @Autowired
    private AssignOrderHandler assignOrderHandler;

    @MockBean
    private CourierRepository courierRepository;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private UnitOfWork unitOfWork;

    @Test
    void apply_whenNoReadyCouriers() {
        // given
        Courier busyCourier = parseFile(COURIER_BUSY_JSON, Courier.class);

        when(courierRepository.getAllReady()).thenReturn(List.of(busyCourier));
        // when
        boolean apply = assignOrderHandler.apply(new AssignOrderCommand());
        // then
        assertFalse(apply);
        verify(orderRepository, never()).getAllAssigned();
        verify(unitOfWork, never()).commit();
    }

    @Test
    void apply_whenNoCreatedOrders() {
        // given
        Courier readyCourier = parseFile(COURIER_READY_JSON, Courier.class);

        when(courierRepository.getAllReady()).thenReturn(List.of(readyCourier));
        when(orderRepository.getAllCreated()).thenReturn(Collections.emptyList());
        // when
        boolean apply = assignOrderHandler.apply(new AssignOrderCommand());
        // then
        assertFalse(apply);
        verify(unitOfWork, never()).registerModified(any());
        verify(unitOfWork, never()).commit();
    }

    @Test
    void apply_whenNoCourierCanHandleOrder() {
        // given
        Courier readyCourier = parseFile(COURIER_READY_JSON, Courier.class);
        Order createdOrder = parseFile(ORDER_CREATED_JSON, Order.class);

        when(courierRepository.getAllReady()).thenReturn(List.of(readyCourier));
        when(orderRepository.getAllCreated()).thenReturn(List.of(createdOrder));
        // when
        boolean apply = assignOrderHandler.apply(new AssignOrderCommand());
        // then
        assertFalse(apply);
        verify(unitOfWork, never()).registerModified(any());
        verify(unitOfWork, never()).commit();
    }

    @Test
    void apply_whenSuccess() {
        // given
        Courier readyCourier = parseFile(COURIER_READY_ON_CAR_JSON, Courier.class);
        when(courierRepository.getAllReady()).thenReturn(List.of(readyCourier));

        Order createdOrder = parseFile(ORDER_CREATED_JSON, Order.class);
        when(orderRepository.getAllCreated()).thenReturn(List.of(createdOrder));
        // when
        boolean apply = assignOrderHandler.apply(new AssignOrderCommand());
        // then
        assertTrue(apply);
        verify(unitOfWork).registerModified(createdOrder);
        verify(unitOfWork).registerModified(readyCourier);
        verify(unitOfWork).commit();
    }
}
