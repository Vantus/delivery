package ru.zhuvar.delivery.core.domain.courier;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;
import ru.zhuvar.delivery.core.domain.sharedkernel.Weight;

import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.zhuvar.delivery.core.domain.courier.Courier.createCourier;

public class CourierTest {

    @MethodSource("moveOneStepToLocationArguments")
    @ParameterizedTest
    public void moveOneStepToLocation_successTest(Courier courier,
                                                  Order orderTo,
                                                  Location expectedLocation
    ) {
        // given
        courier.startWork();
        courier.assignOrder(orderTo);
        // when
        courier.moveOneStepToLocation(orderTo);
        // then
        assertEquals(0, courier.getLocation().distanceTo(expectedLocation));
    }

    public static Stream<Arguments> moveOneStepToLocationArguments() {
        return Stream.of(
                Arguments.of(
                        createCourier("test_ped", Transport.PEDESTRIAN),
                        Order.createOrder(UUID.randomUUID(), new Location(9, 1), new Weight(1)),
                        new Location(2, 1)
                ),
                Arguments.of(
                        createCourier("test_bic", Transport.BICYCLE),
                        Order.createOrder(UUID.randomUUID(), new Location(1, 9), new Weight(1)),
                        new Location(1, 3)
                ),
                Arguments.of(
                        createCourier("test_sco", Transport.SCOOTER),
                        Order.createOrder(UUID.randomUUID(), new Location(2, 9), new Weight(1)),
                        new Location(2, 3)
                ),
                Arguments.of(
                        createCourier("test_car", Transport.CAR),
                        Order.createOrder(UUID.randomUUID(), new Location(3, 5), new Weight(1)),
                        new Location(3, 3)
                ),
                Arguments.of(
                        createCourier("test_ped_onPlace", Transport.PEDESTRIAN),
                        Order.createOrder(UUID.randomUUID(), new Location(1, 1), new Weight(1)),
                        new Location(1, 1)
                )
        );
    }


    @MethodSource("calculateStepsToLocationArguments")
    @ParameterizedTest
    public void calculateStepsToLocation_successTest(Courier courier, Location locationTo, int expectedSteps) {
        // when
        int actualSteps = courier.calculateStepsToLocation(locationTo);
        // then
        assertEquals(expectedSteps, actualSteps);
    }

    public static Stream<Arguments> calculateStepsToLocationArguments() {
        return Stream.of(
                Arguments.of(
                        createCourier("test_ped", Transport.PEDESTRIAN),
                        new Location(5, 5),
                        8
                ),
                Arguments.of(
                        createCourier("test_bic", Transport.BICYCLE),
                        new Location(8, 9),
                        8
                ),
                Arguments.of(
                        createCourier("test_sco", Transport.SCOOTER),
                        new Location(3, 9),
                        4
                ),
                Arguments.of(
                        createCourier("test_car", Transport.CAR),
                        new Location(9, 4),
                        3
                ),
                Arguments.of(
                        createCourier("test_ped_onPlace", Transport.PEDESTRIAN),
                        new Location(1, 1),
                        0
                )
        );
    }
}
