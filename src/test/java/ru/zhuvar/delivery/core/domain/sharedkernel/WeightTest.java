package ru.zhuvar.delivery.core.domain.sharedkernel;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.of;

public class WeightTest {

    @Test
    void createWeight_whenArgumentPositive() {
        assertDoesNotThrow(() -> new Weight(1));
    }

    @ParameterizedTest
    @MethodSource("compareToArguments")
    void compareTo(int w1, int w2, int expected) {
        // given
        Weight weight1 = new Weight(w1);
        Weight weight2 = new Weight(w2);
        // when
        int result = weight1.compareTo(weight2);
        //then
        assertEquals(expected, result);
    }

    static Stream<Arguments> compareToArguments() {
        return Stream.of(
                of(10, 5, 1),
                of(1, 1, 0),
                of(1, 3, -1)
        );
    }
}
