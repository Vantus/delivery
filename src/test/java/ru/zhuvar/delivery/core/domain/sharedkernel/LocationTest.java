package ru.zhuvar.delivery.core.domain.sharedkernel;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.of;

public class LocationTest {

    private static final String X_ILLEGAL_ASSERTION = "x should be in [1, 10]";
    private static final String Y_ILLEGAL_ASSERTION = "y should be in [1, 10]";

    @ParameterizedTest
    @MethodSource("illegalArgumentOnCreateLocation")
    void createLocation_whenFailedOnFieldsValidation(int x, int y, String expectedErrorMessage) {
        // when
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new Location(x, y));
        // then
        assertEquals(expectedErrorMessage, exception.getMessage());
    }

    @Test
    void calculateDistanceTo() {
        // given
        Location from = new Location(1, 3);
        Location to = new Location(5, 1);
        int expected = 6;
        // when
        Integer distance = from.distanceTo(to);
        // when
        assertEquals(expected, distance);
    }

    static Stream<Arguments> illegalArgumentOnCreateLocation() {
        return Stream.of(
                of(0, 0, X_ILLEGAL_ASSERTION),
                of(-1, 2, X_ILLEGAL_ASSERTION),
                of(2, -1, Y_ILLEGAL_ASSERTION),
                of(1, 11, Y_ILLEGAL_ASSERTION),
                of(11, 1, X_ILLEGAL_ASSERTION)
        );
    }

    @ParameterizedTest
    @MethodSource("moveToInputArguments")
    void moveTo(Location locationFrom, Location locationTo, int stepSize, Location expectedLocation) {
        // when
        Location actualLocation = locationFrom.moveTo(locationTo, stepSize);
        // then
        assertEquals(0, expectedLocation.distanceTo(actualLocation));
    }

    static Stream<Arguments> moveToInputArguments() {
        return Stream.of(
                of(new Location(1, 1), new Location(9, 1), 1, new Location(2, 1)),
                of(new Location(1, 1), new Location(1, 9), 2, new Location(1, 3)),
                of(new Location(1, 1), new Location(2, 9), 3, new Location(2, 3)),
                of(new Location(1, 1), new Location(3, 5), 4, new Location(3, 3)),
                of(new Location(1, 1), new Location(1, 1), 1, new Location(1, 1)),
                of(new Location(1, 1), new Location(7, 7), 99, new Location(7, 7))
        );
    }
}
