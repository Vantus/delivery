CREATE TABLE IF NOT EXISTS outbox_message_status
(
    id     smallserial primary key,
    status varchar(255) not null
);

INSERT INTO outbox_message_status
    (id, status)
values (1, 'WAITING'),
       (2, 'DESERIALIZATION_ERROR'),
       (3, 'NETWORK_ERROR'),
       (4, 'ERROR'),
       (5, 'SENT')
on conflict (id) do nothing;


CREATE TABLE IF NOT EXISTS outbox_messages
(
    id                uuid primary key,
    type              varchar(255)                not null,
    message           jsonb                       not null,
    created_at        timestamp without time zone not null default now(),
    modified_at       timestamp without time zone null,
    version           smallint                    not null default 1,
    error_description text                        null,
    status_id         smallserial                 not null
);