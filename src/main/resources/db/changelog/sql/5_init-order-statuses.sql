CREATE TABLE IF NOT EXISTS order_statuses
(
    id     smallserial primary key,
    status varchar(255) not null
);

INSERT INTO order_statuses
    (id, status)
VALUES (1, 'Created'),
       (2, 'Assigned'),
       (3, 'Completed')
ON CONFLICT (id) DO NOTHING;