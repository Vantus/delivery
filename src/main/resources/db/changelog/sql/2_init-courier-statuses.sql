CREATE TABLE IF NOT EXISTS courier_statuses
(
    id   smallserial primary key,
    name varchar(255) not null
);

INSERT INTO courier_statuses
    (id, name)
VALUES (1, 'NotAvailable'),
       (2, 'Ready'),
       (3, 'Busy')
ON CONFLICT (id) DO NOTHING;