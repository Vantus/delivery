CREATE TABLE IF NOT EXISTS courier_transports
(
    id       smallserial primary key,
    name     varchar(255) not null,
    speed    int          not null,
    capacity int          not null
);

INSERT INTO courier_transports
    (id, name, speed, capacity)
VALUES (1, 'Pedestrian', 1, 1),
       (2, 'Bicycle', 2, 4),
       (3, 'Scooter', 3, 6),
       (4, 'Car', 4, 8)
ON CONFLICT (id) DO NOTHING
;