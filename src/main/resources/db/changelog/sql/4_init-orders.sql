CREATE TABLE IF NOT EXISTS orders
(
    id         uuid primary key,
    courier_id uuid,
    location   varchar(255) not null,
    weight     varchar(255) not null,
    status_id  int          not null
);