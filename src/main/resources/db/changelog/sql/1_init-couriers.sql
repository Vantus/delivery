CREATE TABLE IF NOT EXISTS couriers
(
    id           uuid primary key,
    name         varchar(255) not null,
    transport_id int          not null,
    location     varchar(255) not null,
    status_id    int          not null
);
