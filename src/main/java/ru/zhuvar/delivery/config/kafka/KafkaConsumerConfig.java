package ru.zhuvar.delivery.config.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.util.backoff.FixedBackOff;
import ru.zhuvar.delivery.api.adapters.kafka.basket.BasketConfirmedIntegrationEvent;

import java.util.Map;

import static org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG;
import static org.springframework.kafka.support.serializer.JsonDeserializer.VALUE_DEFAULT_TYPE;

@Slf4j
@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    @Value("${spring.kafka.properties.bootstrap.servers}")
    public String bootstrapServers;

    @Value("${spring.kafka.consumer.group-id:DeliveryConsumerGroup}")
    public String groupId;

    @Bean
    public ConsumerFactory<String, BasketConfirmedIntegrationEvent> basketConsumerFactory() {
        return new DefaultKafkaConsumerFactory<>(Map.of(
                BOOTSTRAP_SERVERS_CONFIG, bootstrapServers,
                GROUP_ID_CONFIG, groupId,
                KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class,
                VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class,
                AUTO_OFFSET_RESET_CONFIG, "earliest",
                VALUE_DEFAULT_TYPE, BasketConfirmedIntegrationEvent.class.getName()
        ));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, BasketConfirmedIntegrationEvent> basketKafkaListenerContainerFactory(
            ConsumerFactory<String, BasketConfirmedIntegrationEvent> basketConsumerFactory
    ) {
        ConcurrentKafkaListenerContainerFactory<String, BasketConfirmedIntegrationEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(basketConsumerFactory);
        factory.setCommonErrorHandler(new DefaultErrorHandler(
                (consumerRecord, exception) -> log.error("Failed to consume: {}", consumerRecord.value(), exception),
                new FixedBackOff())

        );
        return factory;
    }
}
