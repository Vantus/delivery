package ru.zhuvar.delivery.config.kafka;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import ru.zhuvar.delivery.infrastructure.adapters.kafka.notification.integrationevents.OrderStatusChangedIntegrationEvent;

import java.util.Map;

import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;

@Configuration
public class KafkaProducerConfig {

    @Value("${spring.kafka.properties.bootstrap.servers}")
    public String bootstrapServers;

    @Bean
    public ProducerFactory<String, OrderStatusChangedIntegrationEvent> orderStatusChangedIntegrationEventProducerFactory() {
        return new DefaultKafkaProducerFactory<>(Map.of(
                BOOTSTRAP_SERVERS_CONFIG, bootstrapServers,
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class,
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class
        ));
    }

    @Bean
    public KafkaTemplate<String, OrderStatusChangedIntegrationEvent> orderStatusChangedIntegrationEventKafkaTemplate(
            ProducerFactory<String, OrderStatusChangedIntegrationEvent> orderStatusChangedIntegrationEventProducerFactory
    ) {
        KafkaTemplate<String, OrderStatusChangedIntegrationEvent> template = new KafkaTemplate<>(orderStatusChangedIntegrationEventProducerFactory);
        template.setDefaultTopic("order.status.changed");
        return template;
    }

}
