package ru.zhuvar.delivery.config.scheduler.config;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.zhuvar.delivery.api.adapters.scheduler.job.AssignOrderJob;

@Configuration
public class AssignOrderJobConfig {

    @Value("${spring.quartz.service-group.assign-order}")
    public String ASSIGN_ORDER_SERVICE_GROUP;

    @Bean
    public JobDetail assignOrderJobDetail() {
        return JobBuilder.newJob(AssignOrderJob.class)
                .withIdentity("assignOrderJob", ASSIGN_ORDER_SERVICE_GROUP)
                .storeDurably(true)
                .requestRecovery(true)
                .build();
    }

    @Bean
    public Trigger assignOrderTrigger(JobDetail assignOrderJobDetail) {
        return TriggerBuilder.newTrigger()
                .forJob(assignOrderJobDetail)
                .withIdentity("assignOrderTrigger", ASSIGN_ORDER_SERVICE_GROUP)
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(5))
                .build();
    }
}
