package ru.zhuvar.delivery.config.scheduler;

import lombok.SneakyThrows;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.List;

import static org.quartz.impl.matchers.GroupMatcher.jobGroupEquals;

@Configuration
public class SchedulerConfiguration {

    @Value("${spring.quartz.service-group.assign-order}")
    public String ASSIGN_ORDER_SERVICE_GROUP;

    @Value("${spring.quartz.service-group.move-couriers}")
    public String MOVE_COURIERS_SERVICE_GROUP;

    @Value("${spring.quartz.service-group.post-outbox-messages}")
    public String POST_OUTBOX_MESSAGES_SERVICE_GROUP;

    @Bean
    public Scheduler scheduler(
            List<Trigger> triggers,
            List<JobDetail> jobDetails,
            SchedulerFactoryBean factory) throws SchedulerException {
        List<String> serviceGroups = List.of(ASSIGN_ORDER_SERVICE_GROUP, MOVE_COURIERS_SERVICE_GROUP, POST_OUTBOX_MESSAGES_SERVICE_GROUP);
        Scheduler scheduler = factory.getScheduler();
        serviceGroups.forEach(group -> revalidateJobs(jobDetails, scheduler, group));
        jobDetails.forEach(jobDetail -> rescheduleTriggers(triggers, scheduler, jobDetail));
        scheduler.start();
        return scheduler;

    }

    @SneakyThrows
    private void rescheduleTriggers(List<Trigger> triggers, Scheduler scheduler, JobDetail jobDetail) {
        for (Trigger trigger : triggers) {
            if (!scheduler.checkExists(trigger.getKey())) {
                if (jobDetail.getKey().getGroup().equals(trigger.getKey().getGroup())) {
                    scheduler.scheduleJob(jobDetail, trigger);
                }
            } else {
                scheduler.rescheduleJob(trigger.getKey(), trigger);
            }
        }

    }

    @SneakyThrows
    private void revalidateJobs(List<JobDetail> jobDetails, Scheduler scheduler, String serviceGroup) {
        List<JobKey> keys = jobDetails.stream().map(JobDetail::getKey).toList();
        for (JobKey jobKey : scheduler.getJobKeys(jobGroupEquals(serviceGroup))) {
            if (keys.contains(jobKey)) {
                scheduler.deleteJob(jobKey);
            }
        }
    }
}
