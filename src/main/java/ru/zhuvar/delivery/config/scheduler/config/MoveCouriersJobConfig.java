package ru.zhuvar.delivery.config.scheduler.config;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.zhuvar.delivery.api.adapters.scheduler.job.MoveCouriersJob;

@Configuration
public class MoveCouriersJobConfig {

    @Value("${spring.quartz.service-group.move-couriers}")
    public String MOVE_COURIERS_SERVICE_GROUP;

    @Bean
    public JobDetail moveCouriersJobDetail() {
        return JobBuilder.newJob(MoveCouriersJob.class)
                .withIdentity("moveCouriersJob", MOVE_COURIERS_SERVICE_GROUP)
                .storeDurably(true)
                .requestRecovery(false)
                .build();
    }

    @Bean
    public Trigger moveCouriersTrigger(JobDetail moveCouriersJobDetail) {
        return TriggerBuilder.newTrigger()
                .forJob(moveCouriersJobDetail)
                .withIdentity("moveCouriersTrigger", MOVE_COURIERS_SERVICE_GROUP)
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(3))
                .build();
    }
}
