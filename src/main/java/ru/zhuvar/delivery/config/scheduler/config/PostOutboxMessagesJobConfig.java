package ru.zhuvar.delivery.config.scheduler.config;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.zhuvar.delivery.api.adapters.scheduler.job.PostOutboxMessagesJob;

@Configuration
public class PostOutboxMessagesJobConfig {

    @Value("${spring.quartz.service-group.post-outbox-messages}")
    private String POST_OUTBOX_MESSAGES_SERVICE_GROUP;

    @Value("${outbox.message.poll.interval}")
    private Integer POST_OUTBOX_MESSAGES_POLL_INTERVAL;

    @Bean
    public JobDetail postOutboxMessagesJobDetails() {
        return JobBuilder.newJob(PostOutboxMessagesJob.class)
                .withIdentity("postOutboxMessagesJob", POST_OUTBOX_MESSAGES_SERVICE_GROUP)
                .storeDurably(true)
                .requestRecovery(false)
                .build();
    }

    @Bean
    public Trigger postOutboxMessagesTrigger(JobDetail postOutboxMessagesJobDetails) {
        return TriggerBuilder.newTrigger()
                .forJob(postOutboxMessagesJobDetails)
                .withIdentity("postOutboxMessagesTrigger", POST_OUTBOX_MESSAGES_SERVICE_GROUP)
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(2))
                .build();
    }

}
