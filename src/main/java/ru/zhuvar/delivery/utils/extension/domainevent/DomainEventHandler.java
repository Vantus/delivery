package ru.zhuvar.delivery.utils.extension.domainevent;

public interface DomainEventHandler<DE extends DomainEvent> {
    void handle(DE domainEvent);
}
