package ru.zhuvar.delivery.utils.extension.domainevent.exception;

public class PublishDomainEventException extends RuntimeException {
    public PublishDomainEventException(String msg) {
        super(msg);
    }
}
