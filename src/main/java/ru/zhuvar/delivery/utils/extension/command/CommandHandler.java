package ru.zhuvar.delivery.utils.extension.command;

public interface CommandHandler<T extends Command> {

    boolean apply(T command);
}
