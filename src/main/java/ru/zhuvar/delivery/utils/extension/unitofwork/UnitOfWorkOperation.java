package ru.zhuvar.delivery.utils.extension.unitofwork;

public enum UnitOfWorkOperation {
    INSERT, DELETE, MODIFY
}
