package ru.zhuvar.delivery.utils.extension.aggregate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.zhuvar.delivery.utils.extension.domainevent.DomainEvent;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public abstract class Aggregate {
    @Getter
    protected UUID id;
    @Builder.Default
    protected List<DomainEvent> events = new LinkedList<>();

    public List<DomainEvent> getEvents() {
        return List.copyOf(events);
    }
}
