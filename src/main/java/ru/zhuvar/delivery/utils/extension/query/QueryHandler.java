package ru.zhuvar.delivery.utils.extension.query;

public interface QueryHandler<T extends Query> {
    QueryResponse<T> apply(T query);
}
