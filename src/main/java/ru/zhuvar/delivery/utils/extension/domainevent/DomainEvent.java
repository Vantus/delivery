package ru.zhuvar.delivery.utils.extension.domainevent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public abstract class DomainEvent {
    protected UUID id;
}
