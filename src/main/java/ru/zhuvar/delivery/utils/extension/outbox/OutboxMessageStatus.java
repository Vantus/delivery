package ru.zhuvar.delivery.utils.extension.outbox;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public enum OutboxMessageStatus {
    WAITING(1L, "Waiting"),
    DESERIALIZATION_ERROR(2L, "DeserializationError"),
    NETWORK_ERROR(3L, "NetworkError"),
    ERROR(4L, "Error"),
    SENT(5L, "Sent");

    private final Long id;
    private final String status;

    public static OutboxMessageStatus findById(Long id) {
        return Arrays.stream(OutboxMessageStatus.values())
                .filter(outboxMessageStatus -> outboxMessageStatus.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("No OutboxMessageStatus found with outboxMessageId %s", id)));
    }
}
