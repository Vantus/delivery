package ru.zhuvar.delivery.utils.extension.outbox;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class OutboxMessage {
    private UUID id;
    private String type;
    private String message;
    private Integer version;
    private OutboxMessageStatus status;
    private String errorDescription;
}
