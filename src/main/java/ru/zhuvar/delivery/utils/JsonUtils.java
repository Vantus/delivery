package ru.zhuvar.delivery.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;

import java.io.File;

import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.io.FileUtils.readFileToString;

public class JsonUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(WRITE_DATES_AS_TIMESTAMPS);
    }

    @SneakyThrows
    public static <T> T parseFile(String filepath, Class<T> castedType) {
        return objectMapper.readValue(getFileAsString(filepath), castedType);
    }

    @SneakyThrows
    public static <T> T parseFile(String filepath, TypeReference<T> reference) {
        return objectMapper.readValue(getFileAsString(filepath), reference);
    }

    @SneakyThrows
    private static String getFileAsString(String filepath) {
        ClassPathResource classPathResource = new ClassPathResource(filepath);
        File file = classPathResource.getFile();
        return readFileToString(file, UTF_8);
    }

    @SneakyThrows
    public static String toJson(Object o) {
        return objectMapper.writeValueAsString(o);
    }

    public static <T> T fromJson(String json, String className) throws ClassNotFoundException, JsonProcessingException {
        Class<?> aClass = Class.forName(className);
        return (T) objectMapper.readValue(json, aClass);
    }
}
