package ru.zhuvar.delivery.api.adapters.http;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.zhuvar.delivery.api.adapters.http.contract.api.ApiApi;
import ru.zhuvar.delivery.api.adapters.http.contract.model.Courier;
import ru.zhuvar.delivery.api.adapters.http.contract.model.Order;
import ru.zhuvar.delivery.api.adapters.http.mapper.CourierDtoMapper;
import ru.zhuvar.delivery.api.adapters.http.mapper.OrderDtoMapper;
import ru.zhuvar.delivery.core.application.commands.createorder.CreateOrderHandler;
import ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders.GetAllCreatedOrAssignedOrdersQuery;
import ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders.GetAllCreatedOrAssignedOrdersQueryHandler;
import ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders.GetAllCreatedOrAssignedOrdersQueryResponse;
import ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers.GetAllReadyOrBusyCouriersQuery;
import ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers.GetAllReadyOrBusyCouriersQueryHandler;
import ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers.GetAllReadyOrBusyCouriersQueryResponse;

import java.util.List;

import static ru.zhuvar.delivery.core.application.commands.createorder.CreateOrderCommandFactory.createRandomOrder;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DefaultApiController implements ApiApi {

    private final CreateOrderHandler createOrderHandler;
    private final GetAllReadyOrBusyCouriersQueryHandler couriersQueryHandler;
    private final GetAllCreatedOrAssignedOrdersQueryHandler ordersQueryHandler;
    private final OrderDtoMapper orderMapper;
    private final CourierDtoMapper courierMapper;

    @Override
    public ResponseEntity<List<Order>> getOrders() {
        log.info("GET: getOrders");
        GetAllCreatedOrAssignedOrdersQueryResponse orders = ordersQueryHandler.apply(new GetAllCreatedOrAssignedOrdersQuery());
        return ResponseEntity.ok(orderMapper.toDtoList(orders.orders()));
    }

    @Override
    public ResponseEntity<List<Courier>> getCouriers() {
        log.info("GET: getCouriers");
        GetAllReadyOrBusyCouriersQueryResponse couriers = couriersQueryHandler.apply(new GetAllReadyOrBusyCouriersQuery());
        return ResponseEntity.ok(courierMapper.toDtoList(couriers.couriers()));
    }

    @Override
    public ResponseEntity<Void> createOrder() {
        log.info("POST: createOrder");
        createOrderHandler.apply(createRandomOrder());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
