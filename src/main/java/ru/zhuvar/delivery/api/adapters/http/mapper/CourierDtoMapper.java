package ru.zhuvar.delivery.api.adapters.http.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.zhuvar.delivery.api.adapters.http.contract.model.Courier;
import ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers.ReadyOrBusyCourier;

import java.util.List;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CourierDtoMapper {

    @Mapping(source = "courierId", target = "id")
    Courier toDto(ReadyOrBusyCourier readyOrBusyCourier);

    List<Courier> toDtoList(List<ReadyOrBusyCourier> readyOrBusyCourierList);
}
