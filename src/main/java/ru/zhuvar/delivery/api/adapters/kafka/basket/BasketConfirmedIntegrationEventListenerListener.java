package ru.zhuvar.delivery.api.adapters.kafka.basket;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import ru.zhuvar.delivery.api.adapters.kafka.basket.mapper.BasketConfirmedEventMapper;
import ru.zhuvar.delivery.core.application.commands.createorder.CreateOrderCommand;
import ru.zhuvar.delivery.core.application.commands.createorder.CreateOrderHandler;

@Slf4j
@Controller
@RequiredArgsConstructor
public class BasketConfirmedIntegrationEventListenerListener {

    private final CreateOrderHandler createOrderHandler;
    private final BasketConfirmedEventMapper basketConfirmedEventMapper;

    @KafkaListener(
            topics = "basket.confirmed",
            groupId = "${spring.kafka.consumer.group-id}",
            containerFactory = "basketKafkaListenerContainerFactory"
    )
    void acceptBasketEvent(@RequestBody @Valid BasketConfirmedIntegrationEvent event) {
        log.info("Kafka listener: acceptBasketEvent: {}", event);
        CreateOrderCommand command = basketConfirmedEventMapper.toCommand(event);
        createOrderHandler.apply(command);
    }
}
