package ru.zhuvar.delivery.api.adapters.scheduler.job;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import ru.zhuvar.delivery.core.application.commands.movecouriers.MoveCouriersCommand;
import ru.zhuvar.delivery.core.application.commands.movecouriers.MoveCouriersHandler;

@Slf4j
@Component
@RequiredArgsConstructor
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class MoveCouriersJob extends QuartzJobBean {

    private final MoveCouriersHandler moveCouriersHandler;

    @Override
    protected void executeInternal(JobExecutionContext context) {
        log.debug("Start: moveCouriers");
        boolean apply = moveCouriersHandler.apply(new MoveCouriersCommand());
        log.debug("Couriers were moved: {}", apply);
    }
}
