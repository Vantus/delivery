package ru.zhuvar.delivery.api.adapters.kafka.basket;

import com.fasterxml.jackson.databind.PropertyNamingStrategies.UpperCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(UpperCamelCaseStrategy.class)
public class BasketConfirmedIntegrationEvent {
    private String basketId;
    private String address;
    private Integer weight;

}
