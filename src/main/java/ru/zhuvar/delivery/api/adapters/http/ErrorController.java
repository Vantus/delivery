package ru.zhuvar.delivery.api.adapters.http;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.zhuvar.delivery.api.adapters.http.contract.model.Error;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Slf4j
@RestControllerAdvice
public class ErrorController {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = INTERNAL_SERVER_ERROR)
    public Error handleException(Exception ex) {
        log.error("Error occurred", ex);
        return new Error(INTERNAL_SERVER_ERROR.value(), ex.getMessage());
    }
}
