package ru.zhuvar.delivery.api.adapters.kafka.basket.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.zhuvar.delivery.api.adapters.kafka.basket.BasketConfirmedIntegrationEvent;
import ru.zhuvar.delivery.core.application.commands.createorder.CreateOrderCommand;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface BasketConfirmedEventMapper {

    @Mapping(target = "basketId", source = "basketId")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "weight", expression = "java(new Weight(event.getWeight()))")
    CreateOrderCommand toCommand(BasketConfirmedIntegrationEvent event);
}
