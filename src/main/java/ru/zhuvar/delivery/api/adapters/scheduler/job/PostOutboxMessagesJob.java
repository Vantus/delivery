package ru.zhuvar.delivery.api.adapters.scheduler.job;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.service.OutboxMessagesService;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessage;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class PostOutboxMessagesJob extends QuartzJobBean {

    @Value("${outbox.message.batch.size}")
    private Integer pollBatchSize;

    private final OutboxMessagesService outboxMessagesService;

    @Override
    protected void executeInternal(JobExecutionContext context) {
        log.info("Start: postOutboxMessages");
        List<OutboxMessage> outboxMessagesUpdated = outboxMessagesService.post(pollBatchSize);
        log.trace("All updated outbox messages: {}", outboxMessagesUpdated);
        log.info("Outbox messages were posted: {}", outboxMessagesUpdated.size());
    }
}
