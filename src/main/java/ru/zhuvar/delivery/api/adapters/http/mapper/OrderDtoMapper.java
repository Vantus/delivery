package ru.zhuvar.delivery.api.adapters.http.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.zhuvar.delivery.api.adapters.http.contract.model.Order;
import ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders.CreatedOrAssignedOrder;

import java.util.List;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface OrderDtoMapper {

    @Mapping(source = "orderId", target = "id")
    Order toDto(CreatedOrAssignedOrder createdOrAssignedOrder);

    List<Order> toDtoList(List<CreatedOrAssignedOrder> createdOrAssignedOrders);
}
