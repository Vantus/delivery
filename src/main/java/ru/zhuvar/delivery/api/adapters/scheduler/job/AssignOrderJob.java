package ru.zhuvar.delivery.api.adapters.scheduler.job;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import ru.zhuvar.delivery.core.application.commands.assignorder.AssignOrderCommand;
import ru.zhuvar.delivery.core.application.commands.assignorder.AssignOrderHandler;

@Slf4j
@Component
@RequiredArgsConstructor
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class AssignOrderJob extends QuartzJobBean {

    private final AssignOrderHandler assignOrderHandler;

    @Override
    protected void executeInternal(JobExecutionContext context) {
        log.debug("Start: assignOrder");
        boolean apply = assignOrderHandler.apply(new AssignOrderCommand());
        log.debug("Order was assigned: {}", apply);
    }
}
