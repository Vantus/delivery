package ru.zhuvar.delivery.infrastructure.adapters.postgres.shared.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.springframework.stereotype.Component;
import ru.zhuvar.delivery.core.domain.sharedkernel.Weight;

@Converter
@Component
public class WeightConverter implements AttributeConverter<Weight, Integer> {
    @Override
    public Integer convertToDatabaseColumn(Weight weightObject) {
        return weightObject.weight();
    }

    @Override
    public Weight convertToEntityAttribute(Integer weight) {
        return new Weight(weight);
    }
}
