package ru.zhuvar.delivery.infrastructure.adapters.postgres.order.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ru.zhuvar.delivery.core.domain.order.OrderStatus;

@Converter
public class OrderStatusConverter implements AttributeConverter<OrderStatus, Long> {
    @Override
    public Long convertToDatabaseColumn(OrderStatus orderStatus) {
        return orderStatus.getId();
    }

    @Override
    public OrderStatus convertToEntityAttribute(Long id) {
        return OrderStatus.findById(id);
    }
}
