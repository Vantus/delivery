package ru.zhuvar.delivery.infrastructure.adapters.kafka.notification;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import ru.zhuvar.delivery.core.domain.order.domainEvents.OrderStatusChangedDomainEvent;
import ru.zhuvar.delivery.core.ports.NotificationEventProducer;
import ru.zhuvar.delivery.infrastructure.adapters.kafka.notification.integrationevents.OrderStatusChangedIntegrationEvent;
import ru.zhuvar.delivery.infrastructure.adapters.kafka.notification.mapper.EventMapper;
import ru.zhuvar.delivery.utils.extension.domainevent.exception.PublishDomainEventException;

import static java.util.concurrent.TimeUnit.SECONDS;

@Slf4j
@Component
@RequiredArgsConstructor
public class NotificationEventProducerImpl implements NotificationEventProducer {

    private final EventMapper eventMapper;
    private final KafkaTemplate<String, OrderStatusChangedIntegrationEvent> kafkaTemplate;

    @Override
    public void publishOrderStatusChangedDomainEvent(OrderStatusChangedDomainEvent event) {
        OrderStatusChangedIntegrationEvent integrationEvent = eventMapper.toIntegrationEvent(event);
        try {
            var f = kafkaTemplate.sendDefault(integrationEvent);
            f.get(10, SECONDS);
        } catch (Exception e) {
            log.error("Unable to send OrderStatusChangedIntegrationEvent: {}", integrationEvent);
            throw new PublishDomainEventException(e.getMessage());
        }
    }

}
