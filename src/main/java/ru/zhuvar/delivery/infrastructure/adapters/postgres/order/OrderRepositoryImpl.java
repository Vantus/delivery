package ru.zhuvar.delivery.infrastructure.adapters.postgres.order;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.domain.order.OrderStatus;
import ru.zhuvar.delivery.core.ports.OrderRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.order.entity.OrderEntity;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.order.jpa.OrderJpaRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.order.mapper.OrderMapper;

import java.util.List;
import java.util.UUID;

import static ru.zhuvar.delivery.core.domain.order.OrderStatus.ASSIGNED;
import static ru.zhuvar.delivery.core.domain.order.OrderStatus.CREATED;

@Repository
@Transactional
@RequiredArgsConstructor
public class OrderRepositoryImpl implements OrderRepository {

    private final OrderMapper mapper;
    private final OrderJpaRepository repository;

    @Override
    public Order add(Order order) {
        OrderEntity orderEntity = mapper.toEntity(order);
        OrderEntity savedEntity = repository.saveAndFlush(orderEntity);
        return mapper.toAggregate(savedEntity);
    }

    @Override
    public void update(Order order) {
        OrderEntity updatedOrder = findByIdOrThrow(order.getId());
        OrderEntity orderEntity = mapper.updateEntity(mapper.toEntity(order), updatedOrder);
        repository.saveAndFlush(orderEntity);
    }

    @Override
    public Order getById(UUID orderId) {
        return mapper.toAggregate(findByIdOrThrow(orderId));
    }

    @Override
    public List<Order> getAllCreated() {
        return getAllByStatus(CREATED);
    }

    @Override
    public List<Order> getAllAssigned() {
        return getAllByStatus(ASSIGNED);
    }

    private List<Order> getAllByStatus(OrderStatus orderStatus) {
        return repository.findAllByStatus(orderStatus).stream()
                .map(mapper::toAggregate)
                .toList();
    }

    private OrderEntity findByIdOrThrow(UUID id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Order with orderId %s not found", id)));
    }
}
