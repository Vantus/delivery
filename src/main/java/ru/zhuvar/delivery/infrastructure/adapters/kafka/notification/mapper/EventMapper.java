package ru.zhuvar.delivery.infrastructure.adapters.kafka.notification.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.zhuvar.delivery.core.domain.order.domainEvents.OrderStatusChangedDomainEvent;
import ru.zhuvar.delivery.infrastructure.adapters.kafka.notification.integrationevents.OrderStatusChangedIntegrationEvent;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface EventMapper {

    @Mapping(target = "orderId", source = "id")
    OrderStatusChangedIntegrationEvent toIntegrationEvent(OrderStatusChangedDomainEvent event);
}
