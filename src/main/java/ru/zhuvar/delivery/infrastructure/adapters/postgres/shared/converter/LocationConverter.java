package ru.zhuvar.delivery.infrastructure.adapters.postgres.shared.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.springframework.stereotype.Component;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;

import static java.lang.Integer.parseInt;

@Converter
@Component
public class LocationConverter implements AttributeConverter<Location, String> {

    private static final String LOCATION_PATTERN = "[%d;%d]";

    @Override
    public String convertToDatabaseColumn(Location location) {
        return LOCATION_PATTERN.formatted(location.x(), location.y());
    }

    @Override
    public Location convertToEntityAttribute(String s) {
        String[] split = s.replaceAll("[\\[\\]]", "").split(";");
        return new Location(parseInt(split[0]), parseInt(split[1]));
    }
}
