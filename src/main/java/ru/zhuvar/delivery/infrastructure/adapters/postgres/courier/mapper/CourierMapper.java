package ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.entity.CourierEntity;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CourierMapper {

    CourierEntity toEntity(Courier courier);

    Courier toAggregate(CourierEntity courierEntity);

    CourierEntity updateEntity(CourierEntity entityToUpdate, @MappingTarget CourierEntity updated);

}
