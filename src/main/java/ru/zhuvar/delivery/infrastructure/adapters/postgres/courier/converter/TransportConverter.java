package ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ru.zhuvar.delivery.core.domain.courier.Transport;

@Converter
public class TransportConverter implements AttributeConverter<Transport, Long> {

    @Override
    public Long convertToDatabaseColumn(Transport transport) {
        return transport.getId();
    }

    @Override
    public Transport convertToEntityAttribute(Long id) {
        return Transport.findById(id);
    }
}
