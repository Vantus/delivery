package ru.zhuvar.delivery.infrastructure.adapters.postgres.order.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.zhuvar.delivery.core.domain.order.OrderStatus;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.order.entity.OrderEntity;

import java.util.List;
import java.util.UUID;

public interface OrderJpaRepository extends JpaRepository<OrderEntity, UUID> {

    List<OrderEntity> findAllByStatus(OrderStatus status);

}
