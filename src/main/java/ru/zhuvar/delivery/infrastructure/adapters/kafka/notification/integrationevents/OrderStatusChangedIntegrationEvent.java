package ru.zhuvar.delivery.infrastructure.adapters.kafka.notification.integrationevents;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import ru.zhuvar.delivery.core.domain.order.OrderStatus;

import java.util.UUID;

@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public record OrderStatusChangedIntegrationEvent(UUID orderId, OrderStatus orderStatus) {
}
