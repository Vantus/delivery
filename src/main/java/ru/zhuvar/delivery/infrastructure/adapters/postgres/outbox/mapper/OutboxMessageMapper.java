package ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.entity.OutboxMessageEntity;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessage;

import java.util.List;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface OutboxMessageMapper {

    OutboxMessageEntity toEntity(OutboxMessage outboxMessage);

    List<OutboxMessageEntity> toEntities(List<OutboxMessage> outboxMessages);

    OutboxMessage fromEntity(OutboxMessageEntity outboxMessageEntity);

    List<OutboxMessage> fromEntities(List<OutboxMessageEntity> outboxMessageEntities);
}
