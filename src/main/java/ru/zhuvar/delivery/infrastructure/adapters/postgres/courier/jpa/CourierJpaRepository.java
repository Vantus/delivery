package ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.zhuvar.delivery.core.domain.courier.CourierStatus;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.entity.CourierEntity;

import java.util.List;
import java.util.UUID;

public interface CourierJpaRepository extends JpaRepository<CourierEntity, UUID> {

    List<CourierEntity> findAllByStatus(CourierStatus status);
}
