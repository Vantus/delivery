package ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.converter.OutboxMessageStatusConverter;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "OUTBOX_MESSAGES")
@EntityListeners(AuditingEntityListener.class)
@DynamicUpdate
@DynamicInsert
public class OutboxMessageEntity {
    @Id
    @Column(name = "ID")
    private UUID id;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "MESSAGE")
    @ColumnTransformer(write = "?::jsonb")
    private String message;

    @Column(name = "VERSION")
    private Integer version = 1;

    @Convert(converter = OutboxMessageStatusConverter.class)
    @Column(name = "STATUS_ID")
    private OutboxMessageStatus status;

    @Column(name = "ERROR_DESCRIPTION")
    private String errorDescription;

    @CreatedDate
    @Column(name = "CREATED_AT", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(name = "MODIFIED_AT")
    private LocalDateTime modifiedAt;

    private

    @PreUpdate
    void onUpdate() {
        this.setVersion(++version);
    }
}
