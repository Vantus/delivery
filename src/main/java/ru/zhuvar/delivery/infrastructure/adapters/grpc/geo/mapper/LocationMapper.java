package ru.zhuvar.delivery.infrastructure.adapters.grpc.geo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface LocationMapper {

    Location fromResponse(ru.zhuvar.delivery.Location location);
}
