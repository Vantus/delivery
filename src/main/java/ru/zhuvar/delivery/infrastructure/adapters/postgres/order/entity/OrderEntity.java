package ru.zhuvar.delivery.infrastructure.adapters.postgres.order.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.zhuvar.delivery.core.domain.order.OrderStatus;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;
import ru.zhuvar.delivery.core.domain.sharedkernel.Weight;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.order.converter.OrderStatusConverter;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.shared.converter.LocationConverter;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.shared.converter.WeightConverter;

import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ORDERS")
public class OrderEntity {

    @Id
    @Column(name = "ID")
    private UUID id;

    @Column(name = "COURIER_ID")
    private UUID courierId;

    @Column(name = "LOCATION")
    @Convert(converter = LocationConverter.class)
    private Location location;

    @Column(name = "WEIGHT")
    @Convert(converter = WeightConverter.class)
    private Weight weight;

    @Column(name = "STATUS_ID")
    @Convert(converter = OrderStatusConverter.class)
    private OrderStatus status;
}
