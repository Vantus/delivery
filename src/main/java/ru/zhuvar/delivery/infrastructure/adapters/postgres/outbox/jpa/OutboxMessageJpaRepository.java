package ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.jpa;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.entity.OutboxMessageEntity;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus;

import java.util.List;
import java.util.UUID;

public interface OutboxMessageJpaRepository extends JpaRepository<OutboxMessageEntity, UUID> {

    List<OutboxMessageEntity> findAllByStatusInOrderByModifiedAtAsc(List<OutboxMessageStatus> status, Pageable pageable);
}
