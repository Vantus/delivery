package ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.zhuvar.delivery.core.ports.OutboxMessageRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.entity.OutboxMessageEntity;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.jpa.OutboxMessageJpaRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.mapper.OutboxMessageMapper;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessage;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus;

import java.util.List;

import static ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus.NETWORK_ERROR;
import static ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus.WAITING;

@Slf4j
@Repository
@Transactional
@RequiredArgsConstructor
public class OutboxMessageRepositoryImpl implements OutboxMessageRepository {

    private final OutboxMessageMapper mapper;
    private final OutboxMessageJpaRepository repository;

    private final List<OutboxMessageStatus> STATUSES_TO_BE_PROCESSES = List.of(WAITING, NETWORK_ERROR);

    @Override
    public OutboxMessage add(OutboxMessage outboxMessage) {
        OutboxMessageEntity entity = mapper.toEntity(outboxMessage);
        OutboxMessageEntity saved = repository.saveAndFlush(entity);
        return mapper.fromEntity(saved);
    }

    @Override
    public List<OutboxMessage> getUnposted(int n) {
        List<OutboxMessageEntity> unposted = repository.findAllByStatusInOrderByModifiedAtAsc(STATUSES_TO_BE_PROCESSES, PageRequest.of(0, n));
        return mapper.fromEntities(unposted);
    }

    @Override
    public List<OutboxMessage> updateAll(List<OutboxMessage> outboxMessages) {
        List<OutboxMessageEntity> entities = mapper.toEntities(outboxMessages);
        List<OutboxMessageEntity> saved = repository.saveAllAndFlush(entities);
        return mapper.fromEntities(saved);
    }


}
