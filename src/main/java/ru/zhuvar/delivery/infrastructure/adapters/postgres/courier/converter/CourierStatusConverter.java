package ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ru.zhuvar.delivery.core.domain.courier.CourierStatus;

@Converter
public class CourierStatusConverter implements AttributeConverter<CourierStatus, Long> {
    @Override
    public Long convertToDatabaseColumn(CourierStatus courierStatus) {
        return courierStatus.getId();
    }

    @Override
    public CourierStatus convertToEntityAttribute(Long id) {
        return CourierStatus.findById(id);
    }
}
