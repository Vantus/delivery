package ru.zhuvar.delivery.infrastructure.adapters.postgres.courier;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.core.ports.CourierRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.entity.CourierEntity;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.jpa.CourierJpaRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.mapper.CourierMapper;

import java.util.List;
import java.util.UUID;

import static ru.zhuvar.delivery.core.domain.courier.CourierStatus.READY;

@Repository
@Transactional
@RequiredArgsConstructor
public class CourierRepositoryImpl implements CourierRepository {

    private final CourierMapper mapper;
    private final CourierJpaRepository repository;

    @Override
    public Courier add(Courier courier) {
        CourierEntity courierEntity = mapper.toEntity(courier);
        CourierEntity savedEntity = repository.saveAndFlush(courierEntity);
        return mapper.toAggregate(savedEntity);
    }

    @Override
    public void update(Courier courier) {
        CourierEntity updatedCourier = findByIdOrThrow(courier.getId());
        CourierEntity courierEntity = mapper.updateEntity(mapper.toEntity(courier), updatedCourier);
        repository.saveAndFlush(courierEntity);
    }

    @Override
    public Courier getById(UUID id) {
        return mapper.toAggregate(findByIdOrThrow(id));
    }

    @Override
    public List<Courier> getAllReady() {
        return repository.findAllByStatus(READY).stream()
                .map(mapper::toAggregate)
                .toList();
    }

    private CourierEntity findByIdOrThrow(UUID id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Courier with orderId %s not found", id)));
    }
}
