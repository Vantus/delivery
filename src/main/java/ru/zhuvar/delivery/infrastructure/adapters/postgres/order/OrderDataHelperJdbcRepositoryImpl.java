package ru.zhuvar.delivery.infrastructure.adapters.postgres.order;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders.CreatedOrAssignedOrder;
import ru.zhuvar.delivery.core.ports.OrderDataHelperRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.shared.converter.LocationConverter;

import java.util.List;
import java.util.UUID;

import static ru.zhuvar.delivery.core.domain.order.OrderStatus.ASSIGNED;
import static ru.zhuvar.delivery.core.domain.order.OrderStatus.CREATED;

@Slf4j
@Repository
@RequiredArgsConstructor
public class OrderDataHelperJdbcRepositoryImpl implements OrderDataHelperRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final LocationConverter locationConverter;

    private static final String SELECT_ORDERS_BY_STATUSES = """
            select id, location
            from orders
            where status_id in (:orderStatusIds);
            """;

    private static final List<Long> CREATED_OR_ASSIGNED_ORDER_STATUS_IDS = List.of(CREATED.getId(), ASSIGNED.getId());

    @Override
    public List<CreatedOrAssignedOrder> getAllCreatedAndAssignedOrders() {
        return jdbcTemplate.query(
                SELECT_ORDERS_BY_STATUSES,
                new MapSqlParameterSource()
                        .addValue("orderStatusIds", CREATED_OR_ASSIGNED_ORDER_STATUS_IDS),
                (rs, rowNumber) -> new CreatedOrAssignedOrder(
                        UUID.fromString(rs.getString("id")),
                        locationConverter.convertToEntityAttribute(rs.getString("location"))
                ));
    }
}
