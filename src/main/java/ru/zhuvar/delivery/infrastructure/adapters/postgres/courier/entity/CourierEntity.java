package ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.zhuvar.delivery.core.domain.courier.CourierStatus;
import ru.zhuvar.delivery.core.domain.courier.Transport;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.converter.CourierStatusConverter;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.courier.converter.TransportConverter;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.shared.converter.LocationConverter;

import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "COURIERS")
public class CourierEntity {

    @Id
    @Column(name = "ID")
    private UUID id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TRANSPORT_ID")
    @Convert(converter = TransportConverter.class)
    private Transport transport;

    @Column(name = "LOCATION")
    @Convert(converter = LocationConverter.class)
    private Location location;

    @Column(name = "STATUS_ID")
    @Convert(converter = CourierStatusConverter.class)
    private CourierStatus status;
}
