package ru.zhuvar.delivery.infrastructure.adapters.postgres.courier;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers.ReadyOrBusyCourier;
import ru.zhuvar.delivery.core.ports.CourierDataHelperRepository;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.shared.converter.LocationConverter;

import java.util.List;
import java.util.UUID;

import static ru.zhuvar.delivery.core.domain.courier.CourierStatus.BUSY;
import static ru.zhuvar.delivery.core.domain.courier.CourierStatus.READY;

@Slf4j
@Repository
@RequiredArgsConstructor
public class CourierDataHelperJdbcRepositoryImpl implements CourierDataHelperRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final LocationConverter locationConverter;

    private static final String SELECT_COURIERS_BY_STATUSES = """
            select id, name, location
            from couriers
            where status_id in (:courierStatusIds);
            """;

    private static final List<Long> READY_OR_BUSY_COURIER_STATUS_IDS = List.of(READY.getId(), BUSY.getId());

    @Override
    public List<ReadyOrBusyCourier> getAllReadyAndBusyCouriers() {
        return jdbcTemplate.query(
                SELECT_COURIERS_BY_STATUSES,
                new MapSqlParameterSource()
                        .addValue("courierStatusIds", READY_OR_BUSY_COURIER_STATUS_IDS),
                (rs, rowNumber) -> new ReadyOrBusyCourier(
                        UUID.fromString(rs.getString("id")),
                        rs.getString("name"),
                        locationConverter.convertToEntityAttribute(rs.getString("location"))
                ));
    }
}
