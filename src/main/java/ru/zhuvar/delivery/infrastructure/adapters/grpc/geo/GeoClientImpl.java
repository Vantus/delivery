package ru.zhuvar.delivery.infrastructure.adapters.grpc.geo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Component;
import ru.zhuvar.delivery.GeoGrpc;
import ru.zhuvar.delivery.GetGeolocationReply;
import ru.zhuvar.delivery.GetGeolocationRequest;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;
import ru.zhuvar.delivery.core.ports.GeoClient;
import ru.zhuvar.delivery.infrastructure.adapters.grpc.geo.mapper.LocationMapper;

@Slf4j
@Component
@RequiredArgsConstructor
public class GeoClientImpl implements GeoClient {

    @GrpcClient("geo")
    public GeoGrpc.GeoBlockingStub geoClient;

    public final LocationMapper locationMapper;

    @Override
    public Location getLocationByAddress(String address) {
        GetGeolocationRequest request = GetGeolocationRequest.newBuilder()
                .setAddress(address)
                .build();
        GetGeolocationReply locationByAddress = geoClient.getGeolocation(request);
        return locationMapper.fromResponse(locationByAddress.getLocation());
    }
}
