package ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.zhuvar.delivery.core.application.domain.event.handlers.DomainEventsResolver;
import ru.zhuvar.delivery.core.ports.OutboxMessageRepository;
import ru.zhuvar.delivery.utils.extension.domainevent.DomainEvent;
import ru.zhuvar.delivery.utils.extension.domainevent.exception.PublishDomainEventException;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessage;

import java.util.List;

import static ru.zhuvar.delivery.utils.JsonUtils.fromJson;
import static ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus.DESERIALIZATION_ERROR;
import static ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus.ERROR;
import static ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus.NETWORK_ERROR;
import static ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus.SENT;

@Slf4j
@Service
@RequiredArgsConstructor
public class OutboxMessagesService {

    private final OutboxMessageRepository outboxMessageRepository;
    private final DomainEventsResolver domainEventsResolver;

    public List<OutboxMessage> post(Integer pollBatchSize) {
        List<OutboxMessage> unposted = outboxMessageRepository.getUnposted(pollBatchSize);
        unposted.forEach(outboxMessage -> {
            try {
                DomainEvent domainEvent = fromJson(outboxMessage.getMessage(), outboxMessage.getType());
                domainEventsResolver.resolve(domainEvent);
                outboxMessage.setStatus(SENT);
            } catch (JsonProcessingException | ClassNotFoundException deserializationException) {
                outboxMessage.setStatus(DESERIALIZATION_ERROR);
                outboxMessage.setErrorDescription(deserializationException.getMessage());
            } catch (PublishDomainEventException networkException) {
                outboxMessage.setStatus(NETWORK_ERROR);
                outboxMessage.setErrorDescription(networkException.getMessage());
            } catch (Exception defaultException) {
                outboxMessage.setStatus(ERROR);
                outboxMessage.setErrorDescription(defaultException.getMessage());
            }
        });
        return outboxMessageRepository.updateAll(unposted);
    }
}
