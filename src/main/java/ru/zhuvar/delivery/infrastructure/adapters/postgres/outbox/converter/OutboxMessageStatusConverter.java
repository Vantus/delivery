package ru.zhuvar.delivery.infrastructure.adapters.postgres.outbox.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus;

@Converter
public class OutboxMessageStatusConverter implements AttributeConverter<OutboxMessageStatus, Long> {
    @Override
    public Long convertToDatabaseColumn(OutboxMessageStatus status) {
        return status.getId();
    }

    @Override
    public OutboxMessageStatus convertToEntityAttribute(Long id) {
        return OutboxMessageStatus.findById(id);
    }
}
