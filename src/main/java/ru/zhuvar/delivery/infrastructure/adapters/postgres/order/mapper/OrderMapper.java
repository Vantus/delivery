package ru.zhuvar.delivery.infrastructure.adapters.postgres.order.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.infrastructure.adapters.postgres.order.entity.OrderEntity;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface OrderMapper {

    OrderEntity toEntity(Order order);

    Order toAggregate(OrderEntity orderEntity);

    OrderEntity updateEntity(OrderEntity entityToUpdate, @MappingTarget OrderEntity updated);
}
