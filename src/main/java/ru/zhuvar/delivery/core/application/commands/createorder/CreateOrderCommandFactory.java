package ru.zhuvar.delivery.core.application.commands.createorder;

import java.time.LocalDateTime;
import java.util.UUID;

import static ru.zhuvar.delivery.core.domain.courier.Transport.CAR;
import static ru.zhuvar.delivery.core.domain.sharedkernel.Weight.randomWeight;

public class CreateOrderCommandFactory {

    private static final Integer MAX_WEIGHT = CAR.getCapacity().weight();

    public static CreateOrderCommand createRandomOrder() {
        return new CreateOrderCommand(
                UUID.randomUUID(),
                getRandomAddress(),
                randomWeight(MAX_WEIGHT)
        );
    }

    private static String getRandomAddress() {
        return "Address in " + LocalDateTime.now();
    }
}
