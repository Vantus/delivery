package ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.zhuvar.delivery.core.ports.CourierDataHelperRepository;
import ru.zhuvar.delivery.utils.extension.query.QueryHandler;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GetAllReadyOrBusyCouriersQueryHandler implements QueryHandler<GetAllReadyOrBusyCouriersQuery> {

    private final CourierDataHelperRepository courierDataHelperRepository;

    @Override
    public GetAllReadyOrBusyCouriersQueryResponse apply(GetAllReadyOrBusyCouriersQuery query) {
        List<ReadyOrBusyCourier> allReadyOrBusyCouriers = courierDataHelperRepository.getAllReadyAndBusyCouriers();
        return new GetAllReadyOrBusyCouriersQueryResponse(allReadyOrBusyCouriers);
    }
}
