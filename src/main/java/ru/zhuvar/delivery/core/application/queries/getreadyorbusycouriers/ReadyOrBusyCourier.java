package ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers;

import ru.zhuvar.delivery.core.domain.sharedkernel.Location;

import java.util.UUID;

public record ReadyOrBusyCourier(UUID courierId, String name, Location location) {
}
