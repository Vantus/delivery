package ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers;

import ru.zhuvar.delivery.utils.extension.query.QueryResponse;

import java.util.List;

public record GetAllReadyOrBusyCouriersQueryResponse(
        List<ReadyOrBusyCourier> couriers) implements QueryResponse<GetAllReadyOrBusyCouriersQuery> {
}
