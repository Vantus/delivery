package ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.zhuvar.delivery.core.ports.OrderDataHelperRepository;
import ru.zhuvar.delivery.utils.extension.query.QueryHandler;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GetAllCreatedOrAssignedOrdersQueryHandler implements QueryHandler<GetAllCreatedOrAssignedOrdersQuery> {

    private final OrderDataHelperRepository orderDataHelperRepository;

    @Override
    public GetAllCreatedOrAssignedOrdersQueryResponse apply(GetAllCreatedOrAssignedOrdersQuery query) {
        List<CreatedOrAssignedOrder> createdOrAssignedOrders = orderDataHelperRepository.getAllCreatedAndAssignedOrders();
        return new GetAllCreatedOrAssignedOrdersQueryResponse(createdOrAssignedOrders);
    }
}
