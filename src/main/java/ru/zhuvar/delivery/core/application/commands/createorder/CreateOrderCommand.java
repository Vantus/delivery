package ru.zhuvar.delivery.core.application.commands.createorder;

import ru.zhuvar.delivery.core.domain.sharedkernel.Weight;
import ru.zhuvar.delivery.utils.extension.command.Command;

import java.util.Objects;
import java.util.UUID;

public record CreateOrderCommand(UUID basketId, String address, Weight weight) implements Command {

    public CreateOrderCommand {
        if (Objects.isNull(basketId)) {
            throw new IllegalArgumentException("basketId should not be null");
        }
        if (Objects.isNull(address)) {
            throw new IllegalArgumentException("address should not be null");
        }
        if (Objects.isNull(weight)) {
            throw new IllegalArgumentException("weight should not be null");
        }
    }
}
