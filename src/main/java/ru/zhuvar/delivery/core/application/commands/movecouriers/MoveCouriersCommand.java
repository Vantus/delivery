package ru.zhuvar.delivery.core.application.commands.movecouriers;

import ru.zhuvar.delivery.utils.extension.command.Command;

public record MoveCouriersCommand() implements Command {
}
