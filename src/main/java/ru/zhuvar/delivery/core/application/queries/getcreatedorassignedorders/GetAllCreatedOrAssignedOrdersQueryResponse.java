package ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders;

import ru.zhuvar.delivery.utils.extension.query.QueryResponse;

import java.util.List;

public record GetAllCreatedOrAssignedOrdersQueryResponse(
        List<CreatedOrAssignedOrder> orders) implements QueryResponse<GetAllCreatedOrAssignedOrdersQuery> {
}
