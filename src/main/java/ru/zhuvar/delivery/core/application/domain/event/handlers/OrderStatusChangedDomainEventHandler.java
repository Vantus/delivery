package ru.zhuvar.delivery.core.application.domain.event.handlers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.zhuvar.delivery.core.domain.order.domainEvents.OrderStatusChangedDomainEvent;
import ru.zhuvar.delivery.core.ports.NotificationEventProducer;
import ru.zhuvar.delivery.utils.extension.domainevent.DomainEventHandler;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderStatusChangedDomainEventHandler implements DomainEventHandler<OrderStatusChangedDomainEvent> {

    private final NotificationEventProducer notificationEventProducer;

    @Override
    public void handle(OrderStatusChangedDomainEvent domainEvent) {
        notificationEventProducer.publishOrderStatusChangedDomainEvent(domainEvent);
    }
}
