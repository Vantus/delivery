package ru.zhuvar.delivery.core.application.commands.assignorder;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.domainservices.DispatchService;
import ru.zhuvar.delivery.core.ports.CourierRepository;
import ru.zhuvar.delivery.core.ports.OrderRepository;
import ru.zhuvar.delivery.core.ports.UnitOfWork;
import ru.zhuvar.delivery.utils.extension.command.CommandHandler;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class AssignOrderHandler implements CommandHandler<AssignOrderCommand> {

    private final CourierRepository courierRepository;
    private final OrderRepository orderRepository;
    private final DispatchService dispatchService;
    private final UnitOfWork unitOfWork;

    @Override
    @Transactional
    public boolean apply(AssignOrderCommand assignOrderCommand) {
        List<Courier> allReadyCouriers = courierRepository.getAllReady();
        if (allReadyCouriers.isEmpty()) {
            log.info("No ready couriers to take order");
            return false;
        }
        List<Order> allCreated = orderRepository.getAllCreated();
        if (allCreated.isEmpty()) {
            log.info("No created orders to be assigned");
            return false;
        }
        Order orderToBeAssigned = allCreated.getFirst();
        log.info("Order to be assigned orderId: {}", orderToBeAssigned.getId());

        try {
            Courier dispatchCourier = dispatchService.dispatch(orderToBeAssigned, allReadyCouriers);
            log.info("Courier {} will start on order: {}", dispatchCourier.getId(), orderToBeAssigned.getId());
            dispatchCourier.assignOrder(orderToBeAssigned);
            orderToBeAssigned.assignToCourier(dispatchCourier.getId());
            unitOfWork.registerModified(orderToBeAssigned);
            unitOfWork.registerModified(dispatchCourier);
        } catch (Exception ex) {
            log.error("Error during dispatching process for order orderId: {}", orderToBeAssigned.getId(), ex);
            return false;
        }
        unitOfWork.commit();
        return true;
    }
}
