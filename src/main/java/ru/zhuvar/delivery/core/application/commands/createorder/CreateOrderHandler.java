package ru.zhuvar.delivery.core.application.commands.createorder;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.ports.GeoClient;
import ru.zhuvar.delivery.core.ports.UnitOfWork;
import ru.zhuvar.delivery.utils.extension.command.CommandHandler;

import static ru.zhuvar.delivery.core.domain.order.Order.createOrder;

@Slf4j
@Service
@RequiredArgsConstructor
public class CreateOrderHandler implements CommandHandler<CreateOrderCommand> {

    private final GeoClient geoClient;
    private final UnitOfWork unitOfWork;

    @Override
    public boolean apply(CreateOrderCommand createOrderCommand) {
        Order order = createOrder(
                createOrderCommand.basketId(),
                geoClient.getLocationByAddress(createOrderCommand.address()),
                createOrderCommand.weight()
        );
        log.info("Create order {}", order);
        unitOfWork.registerNew(order);
        unitOfWork.commit();
        return true;
    }
}
