package ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders;

import ru.zhuvar.delivery.core.domain.sharedkernel.Location;

import java.util.UUID;

public record CreatedOrAssignedOrder(UUID orderId, Location location) {
}
