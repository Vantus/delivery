package ru.zhuvar.delivery.core.application.domain.event.handlers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.zhuvar.delivery.core.domain.order.domainEvents.OrderStatusChangedDomainEvent;
import ru.zhuvar.delivery.utils.extension.domainevent.DomainEvent;

@Slf4j
@Service
@RequiredArgsConstructor
public class DomainEventsResolver {

    private final OrderStatusChangedDomainEventHandler orderStatusChangedDomainEventHandler;

    public void resolve(DomainEvent event) {
        switch (event) {
            case OrderStatusChangedDomainEvent orderStatusChangedDomainEvent ->
                    orderStatusChangedDomainEventHandler.handle(orderStatusChangedDomainEvent);
            case null -> throw new IllegalArgumentException("Event should not be null");
            default -> log.info("Unresolved event {}", event);
        }

    }
}
