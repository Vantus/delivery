package ru.zhuvar.delivery.core.application.commands.movecouriers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.ports.CourierRepository;
import ru.zhuvar.delivery.core.ports.OrderRepository;
import ru.zhuvar.delivery.core.ports.UnitOfWork;
import ru.zhuvar.delivery.utils.extension.command.CommandHandler;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class MoveCouriersHandler implements CommandHandler<MoveCouriersCommand> {

    private final CourierRepository courierRepository;
    private final OrderRepository orderRepository;
    private final UnitOfWork unitOfWork;

    @Override
    public boolean apply(MoveCouriersCommand command) {
        List<Order> assignedOrders = orderRepository.getAllAssigned();
        if (assignedOrders.isEmpty()) {
            log.info("No assigned orders");
            return false;
        }
        assignedOrders.forEach(order -> {
            Courier courier = courierRepository.getById(order.getCourierId());
            boolean isFinish = courier.moveOneStepToLocation(order);
            if (isFinish) {
                log.info("Courier {} finished the order {}", courier.getId(), order.getId());
                order.finishOrder();
                unitOfWork.registerModified(order);
            }
            unitOfWork.registerModified(courier);
        });
        unitOfWork.commit();
        return true;
    }

}
