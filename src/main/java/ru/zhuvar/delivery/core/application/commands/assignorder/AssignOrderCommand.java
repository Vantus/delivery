package ru.zhuvar.delivery.core.application.commands.assignorder;

import ru.zhuvar.delivery.utils.extension.command.Command;

public record AssignOrderCommand() implements Command {
}
