package ru.zhuvar.delivery.core.domain.order;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import ru.zhuvar.delivery.core.domain.order.domainEvents.OrderStatusChangedDomainEvent;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;
import ru.zhuvar.delivery.core.domain.sharedkernel.Weight;
import ru.zhuvar.delivery.utils.extension.aggregate.Aggregate;

import java.util.UUID;

import static java.util.Objects.isNull;
import static ru.zhuvar.delivery.core.domain.order.OrderStatus.ASSIGNED;
import static ru.zhuvar.delivery.core.domain.order.OrderStatus.COMPLETED;
import static ru.zhuvar.delivery.core.domain.order.OrderStatus.CREATED;

/**
 * Заказ
 */
@Slf4j
@Getter
@ToString
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Order extends Aggregate {

    @NotNull
    private UUID courierId;

    @Valid
    @NotNull
    private Location location;

    @Valid
    @NotNull
    private Weight weight;

    @Valid
    @NotNull
    private OrderStatus status;

    /**
     * Создает заказ
     * <p><b>Является единственным валидным способом создания заказа с точки зрения бизнес логики<b/>
     *
     * @param id       идентификатор заказа
     * @param location местоположение, куда нужно доставить заказ
     * @param weight   вес заказа
     * @return заказ с {@code orderId == null} и {@code status == CREATED}
     */
    public static Order createOrder(@NotNull final UUID id, @NotNull final Location location, @NotNull final Weight weight) {
        if (isNull(id)) {
            throw new IllegalArgumentException("Input orderId must not be null");
        }
        Order order = Order.builder()
                .id(id)
                .location(location)
                .weight(weight)
                .courierId(null)
                .status(CREATED)
                .build();
        order.events.add(new OrderStatusChangedDomainEvent(id, CREATED));
        return order;
    }

    public void assignToCourier(@NotNull final UUID courierId) {
        this.status = ASSIGNED;
        this.events.add(new OrderStatusChangedDomainEvent(this.id, this.status));
        this.courierId = courierId;
    }

    public void finishOrder() {
        if (!this.status.equals(ASSIGNED)) {
            throw new IllegalStateException("For finishing order it should be " + ASSIGNED);
        }
        this.status = COMPLETED;
        this.events.add(new OrderStatusChangedDomainEvent(this.id, this.status));
    }


}
