package ru.zhuvar.delivery.core.domain.courier;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Статус курьера
 */
@Getter
@RequiredArgsConstructor
public enum CourierStatus {
    NOT_AVAILABLE(1L, "NotAvailable"),
    READY(2L, "Ready"),
    BUSY(3L, "Busy");

    private final Long id;
    private final String status;

    public static CourierStatus findById(Long id) {
        return Arrays.stream(CourierStatus.values())
                .filter(courierStatus -> courierStatus.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("No CourierStatus found with orderId %s", id)));
    }
}
