package ru.zhuvar.delivery.core.domain.sharedkernel;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import ru.zhuvar.delivery.utils.RandomUtils;

import java.util.Objects;


/**
 * Вес
 */
public record Weight(
        @Positive(message = "weight should be greater than 0") int weight
) implements Comparable<Weight> {

    public static final int MIN_WEIGHT = 1;

    public Weight(int weight) {
        if (weight < MIN_WEIGHT) {
            throw new IllegalArgumentException("weight should be greater than 0");
        }
        this.weight = weight;
    }

    /**
     * Сравниваем текущий вес с переданным
     *
     * @param comparedWeight сравниваемый вес
     * @return 0 если веса равны; 1 вес меньше текущего; -1 вес больше текущего
     * @throws IllegalArgumentException если {@code comparedWeight == null}
     */
    @Override
    public int compareTo(@NotNull final Weight comparedWeight) {
        if (Objects.isNull(comparedWeight)) {
            throw new IllegalArgumentException("Compared weight con not be null");
        }
        return Integer.compare(this.weight(), comparedWeight.weight());
    }

    public static Weight randomWeight(int maxWeight) {
        return new Weight(RandomUtils.getRandomIntInBounds(MIN_WEIGHT, maxWeight));
    }
}
