package ru.zhuvar.delivery.core.domain.courier;

import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;
import ru.zhuvar.delivery.utils.extension.aggregate.Aggregate;

import java.util.UUID;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static ru.zhuvar.delivery.core.domain.courier.CourierStatus.BUSY;
import static ru.zhuvar.delivery.core.domain.courier.CourierStatus.NOT_AVAILABLE;
import static ru.zhuvar.delivery.core.domain.courier.CourierStatus.READY;

/**
 * Курьер
 */
@Slf4j
@Getter
@ToString
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Courier extends Aggregate {
    private String name;
    private Transport transport;
    private Location location;
    private CourierStatus status;


    private static final Location INIT_LOCATION = new Location(1, 1);

    /**
     * Создает курьера
     * <p><b>Является единственным валидным способом создания курьера с точки зрения бизнес логики<b/>
     *
     * @param name      имя курьера
     * @param transport транспорт курьера
     * @return курьер с {@code status == NOT_AVAILABLE} и {@code location == [1; 1]}
     */
    public static Courier createCourier(@NotNull final String name, @NotNull final Transport transport) {
        if (isBlank(name)) {
            throw new IllegalArgumentException("Courier name must not be blank");
        }
        if (isNull(transport)) {
            throw new IllegalArgumentException("Courier transport should not be null");
        }
        return Courier.builder()
                .id(UUID.randomUUID())
                .name(name)
                .transport(transport)
                .location(INIT_LOCATION)
                .status(NOT_AVAILABLE)
                .build();
    }


    /**
     * Начинает рабочий день курьера
     * <p>Переводит курьера в статус {@code READY}
     *
     * @throws IllegalStateException если курьер находится не в статусе {@code NOT_AVAILABLE}
     */
    public void startWork() {
        if (!NOT_AVAILABLE.equals(this.status)) {
            throw new IllegalStateException(String.format("Courier can not start work. Should be %s, now %s", NOT_AVAILABLE, this.status));
        }
        this.status = READY;
    }

    /**
     * Заканчивает рабочий день курьера
     * <p>Переводит курьера в статус {@code NOT_AVAILABLE}
     *
     * @throws IllegalStateException если курьер не находится в статусе {@code BUSY}
     */
    public void endWork() {
        if (!READY.equals(this.status)) {
            throw new IllegalStateException(String.format("Courier can not end work. Should be %s, now %s", READY, this.status));
        }
        this.status = NOT_AVAILABLE;
    }


    /**
     * Назначить заказ на курьера
     * <p>Переводит курьера в статус {@code BUSY}
     *
     * @param order назначаемый на курьера заказ
     * @throws IllegalArgumentException если {@code order == null}
     * @throws IllegalStateException    если курьер не находится в статусе {@code READY}
     */
    public void assignOrder(@NotNull final Order order) {
        if (isNull(order)) {
            throw new IllegalArgumentException("Assigned order can not be null");
        }
        if (!READY.equals(this.status)) {
            throw new IllegalStateException(String.format("Order with orderId %s can not be assigned to courier. Should be %s, new %s", order.getId(), READY, this.status));
        }
        order.assignToCourier(this.getId());
        this.status = BUSY;
    }

    /**
     * Совершает один шаг до переданной локации
     * <p> Изменяет локацию курьера на новую
     * <p> Двигается по принципу - сначала полностью X, потом закрывать Y
     *
     * @param order Заказ, до которого нужно сделать шаг
     * @return {@code boolean} {@code true} если курьер дошел до {@code locationTo}, иначе - {@code false}
     * @throws IllegalArgumentException если {@code location == null}
     * @throws IllegalArgumentException если Курьер уже назначен на другой заказ
     * @throws IllegalArgumentException если текущий статус курьера не {@code BUSY}
     */
    public boolean moveOneStepToLocation(@NotNull final Order order) {
        if (!BUSY.equals(this.getStatus())) {
            throw new IllegalArgumentException(String.format("Courier should be %s", BUSY));
        }
        if (!this.id.equals(order.getCourierId())) {
            throw new IllegalArgumentException("Courier already assigned on another order");
        }
        this.location = this.location.moveTo(order.getLocation(), this.getTransport().getSpeed());
        log.info("For courier {} after moving to {} new Location is {}", this.getId(), order.getLocation(), this.location);
        if (this.location.distanceTo(order.getLocation()) == 0) {
            this.status = READY;
            return true;
        }
        return false;
    }

    /**
     * Рассчитывает количество шагов от текущей локации курьера до переданной
     *
     * @param location локация до которой необходимо рассчитать количество шагов
     * @return {@code int} кольчецов шагов до переданной локации
     * @throws IllegalArgumentException если {@code location == null}
     */
    public int calculateStepsToLocation(@NotNull final Location location) {
        if (isNull(location)) {
            throw new IllegalStateException("Input location can not be null");
        }
        Integer distance = this.location.distanceTo(location);
        return (int) Math.ceil((double) distance / this.transport.getSpeed());
    }

}
