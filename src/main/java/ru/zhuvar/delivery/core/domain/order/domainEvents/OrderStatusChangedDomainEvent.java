package ru.zhuvar.delivery.core.domain.order.domainEvents;

import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.zhuvar.delivery.core.domain.order.OrderStatus;
import ru.zhuvar.delivery.utils.extension.domainevent.DomainEvent;

import java.util.UUID;

@Getter
@NoArgsConstructor
public class OrderStatusChangedDomainEvent extends DomainEvent {

    private OrderStatus orderStatus;

    public OrderStatusChangedDomainEvent(UUID orderId, OrderStatus orderStatus) {
        super(orderId);
        this.orderStatus = orderStatus;
    }
}
