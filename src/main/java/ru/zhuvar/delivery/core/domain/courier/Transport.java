package ru.zhuvar.delivery.core.domain.courier;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.zhuvar.delivery.core.domain.sharedkernel.Weight;

import java.util.Arrays;
import java.util.Objects;

/**
 * Транспорт курьера
 */
@Getter
@RequiredArgsConstructor
public enum Transport {

    PEDESTRIAN(1L, "Pedestrian", 1, new Weight(1)),
    BICYCLE(2L, "Bicycle", 2, new Weight(4)),
    SCOOTER(3L, "Scooter", 3, new Weight(6)),
    CAR(4L, "Car", 4, new Weight(8));

    private final Long id;
    private final String name;
    private final Integer speed;
    private final Weight capacity;

    public static Transport findById(Long id) {
        return Arrays.stream(Transport.values())
                .filter(transport -> transport.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("No Transport found with orderId %s", id)));
    }

    /**
     * Метод, отвечающий на вопрос "может ли данный транспорт перевезти определенный вес?"
     *
     * @param weight вес, проверяемый на возможность быть перевезенным
     * @return {@code true} если переданный вес может быть перевез. Иначе {@code false}
     * @throws IllegalArgumentException если {@code weight == null}
     */
    public boolean isPossibleToTransport(@NotNull Weight weight) {
        if (Objects.isNull(weight)) {
            throw new IllegalArgumentException("Compared weight must not be null");
        }
        return capacity.compareTo(weight) >= 0;
    }
}
