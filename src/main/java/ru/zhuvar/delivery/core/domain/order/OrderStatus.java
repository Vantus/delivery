package ru.zhuvar.delivery.core.domain.order;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * Статус заказа
 */
@Getter
@RequiredArgsConstructor
public enum OrderStatus {
    CREATED(1L, "Created"),
    ASSIGNED(2L, "Assigned"),
    COMPLETED(3L, "Completed");

    private final Long id;
    private final String status;

    public static OrderStatus findById(Long id) {
        return Arrays.stream(OrderStatus.values())
                .filter(orderStatus -> orderStatus.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("No OrderStatus found with orderId %s", id)));
    }
}
