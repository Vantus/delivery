package ru.zhuvar.delivery.core.domain.sharedkernel;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

import java.util.Objects;

import static java.lang.Math.abs;
import static java.util.Objects.isNull;
import static ru.zhuvar.delivery.utils.RandomUtils.getRandomIntInBounds;

/**
 * Координата на доске
 *
 * @param x координата по оси X (горизонталь)
 * @param y координата по оси Y (вертикаль)
 */
public record Location(
        @Min(value = 1, message = "x should not be less than 1") @Max(value = 10, message = "x should not be greater than 10") int x,
        @Min(value = 1, message = "y should not be less than 1") @Max(value = 10, message = "y should not be greater than 10") int y
) {

    public static final int MIN_COORDINATE = 1;
    public static final int MAX_COORDINATE = 10;

    public Location(int x, int y) {
        if (x < MIN_COORDINATE || MAX_COORDINATE < x) {
            throw new IllegalArgumentException("x should be in [%d, %d]".formatted(MIN_COORDINATE, MAX_COORDINATE));
        }
        if (y < MIN_COORDINATE || MAX_COORDINATE < y) {
            throw new IllegalArgumentException("y should be in [%d, %d]".formatted(MIN_COORDINATE, MAX_COORDINATE));
        }
        this.x = x;
        this.y = y;
    }

    /**
     * Находит расстояние между текущей локацией и переданной
     *
     * @param loc сравниваемая локация
     * @return расстояние между текущей и переданной точкой
     * @throws IllegalArgumentException если {@code loc == null}
     */
    public Integer distanceTo(@NotNull final Location loc) {
        if (Objects.isNull(loc)) {
            throw new IllegalArgumentException("Compared location is null");
        }
        return abs(this.x - loc.x()) + abs(this.y() - loc.y());
    }

    /**
     * Совершает один шаг до переданной локации
     * <p> Изменяет локацию курьера на новую
     * <p> Двигается по принципу - сначала полностью X, потом закрывать Y
     *
     * @param locationTo локация до которой необходимо выполнить шаг
     * @param stepSize   максимальный размер шага
     * @throws IllegalArgumentException если {@code location == null}
     */
    public Location moveTo(@NotNull final Location locationTo, int stepSize) {
        if (isNull(locationTo)) {
            throw new IllegalArgumentException("Input location can not be null");
        }
        int availableDistance = stepSize;
        int curX = this.x();
        int curY = this.y();

        int signedDeltaX = locationTo.x() - curX;
        if (abs(signedDeltaX) != 0) {
            int stepDeltaX = Math.min(abs(signedDeltaX), availableDistance);
            curX += (signedDeltaX > 0 ? stepDeltaX : -stepDeltaX);
            availableDistance -= stepDeltaX;
        }
        int signedDeltaY = locationTo.y() - curY;
        if (abs(signedDeltaY) != 0) {
            int stepDeltaY = Math.min(abs(signedDeltaY), availableDistance);
            curY += (signedDeltaY > 0 ? stepDeltaY : -stepDeltaY);
        }
        return new Location(curX, curY);
    }

    public static Location randomLocation() {
        return new Location(
                getRandomIntInBounds(MIN_COORDINATE, MAX_COORDINATE),
                getRandomIntInBounds(MIN_COORDINATE, MAX_COORDINATE)
        );
    }
}
