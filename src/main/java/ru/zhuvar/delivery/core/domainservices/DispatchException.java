package ru.zhuvar.delivery.core.domainservices;

public class DispatchException extends Exception {
    public DispatchException(String message) {
        super(message);
    }
}
