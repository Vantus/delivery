package ru.zhuvar.delivery.core.domainservices;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.core.domain.order.Order;

import java.util.List;

public interface DispatchService {

    Courier dispatch(@NotNull Order order, @NotEmpty List<Courier> couriers) throws DispatchException;
}
