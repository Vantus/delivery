package ru.zhuvar.delivery.core.domainservices;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.core.domain.order.Order;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DispatchServiceImpl implements DispatchService {

    @Override
    public Courier dispatch(@NotNull Order order, @NotEmpty List<Courier> couriers) throws DispatchException {

        List<Courier> courierPossibleToHandleByWeight = couriers.stream()
                .filter(courier -> courier.getTransport().isPossibleToTransport(order.getWeight()))
                .toList();

        if (courierPossibleToHandleByWeight.isEmpty()) {
            log.info("No couriers can handle order: {}", order);
            throw new DispatchException("No couriers can handle order " + order);
        }

        Map<Integer, List<Courier>> collectBySteps = courierPossibleToHandleByWeight.stream()
                .collect(Collectors.groupingBy(courier -> courier.calculateStepsToLocation(order.getLocation())));

        return Collections.min(collectBySteps.entrySet(), Map.Entry.comparingByKey()).getValue().getFirst();
    }
}
