package ru.zhuvar.delivery.core.ports;

import jakarta.validation.constraints.NotNull;
import ru.zhuvar.delivery.core.domain.sharedkernel.Location;

public interface GeoClient {
    Location getLocationByAddress(@NotNull String address);
}
