package ru.zhuvar.delivery.core.ports;

import ru.zhuvar.delivery.core.domain.courier.Courier;

import java.util.List;
import java.util.UUID;

public interface CourierRepository {
    Courier add(Courier courier);

    void update(Courier courier);

    Courier getById(UUID id);

    List<Courier> getAllReady();
}
