package ru.zhuvar.delivery.core.ports;

import ru.zhuvar.delivery.core.domain.order.Order;

import java.util.List;
import java.util.UUID;

public interface OrderRepository {
    Order add(Order order);

    void update(Order order);

    Order getById(UUID orderId);

    List<Order> getAllCreated();

    List<Order> getAllAssigned();
}
