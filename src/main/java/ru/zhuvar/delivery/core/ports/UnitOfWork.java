package ru.zhuvar.delivery.core.ports;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.zhuvar.delivery.core.domain.courier.Courier;
import ru.zhuvar.delivery.core.domain.order.Order;
import ru.zhuvar.delivery.utils.extension.aggregate.Aggregate;
import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessage;
import ru.zhuvar.delivery.utils.extension.unitofwork.UnitOfWorkOperation;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static ru.zhuvar.delivery.utils.JsonUtils.toJson;
import static ru.zhuvar.delivery.utils.extension.outbox.OutboxMessageStatus.WAITING;
import static ru.zhuvar.delivery.utils.extension.unitofwork.UnitOfWorkOperation.DELETE;
import static ru.zhuvar.delivery.utils.extension.unitofwork.UnitOfWorkOperation.INSERT;
import static ru.zhuvar.delivery.utils.extension.unitofwork.UnitOfWorkOperation.MODIFY;

@Slf4j
@Component
@Transactional
@RequiredArgsConstructor
public class UnitOfWork {

    private static final Map<UnitOfWorkOperation, List<Object>> context = Map.of(
            INSERT, new LinkedList<>(),
            MODIFY, new LinkedList<>(),
            DELETE, new LinkedList<>()
    );

    private final CourierRepository courierRepository;
    private final OrderRepository orderRepository;
    private final OutboxMessageRepository outboxMessageRepository;

    public void registerNew(Aggregate aggregate) {
        context.get(INSERT).add(aggregate);
        registerEvents(aggregate, INSERT);
    }

    public void registerModified(Aggregate aggregate) {
        context.get(MODIFY).add(aggregate);
        registerEvents(aggregate, MODIFY);
    }

    public void commit() {
        if (context.isEmpty()) {
            return;
        }
        log.debug("Commit started");
        if (context.containsKey(INSERT)) {
            log.trace("On UnitOfWorkCommit [INSERT: {}]", context.get(INSERT));
            commitInsert();
        }
        if (context.containsKey(MODIFY)) {
            log.trace("On UnitOfWorkCommit [MODIFY: {}]", context.get(MODIFY));
            commitModify();
        }
        if (context.containsKey(DELETE)) {
            log.trace("On UnitOfWorkCommit [DELETE: {}]", context.get(DELETE));
            commitDelete();
        }
        log.debug("commitFinished");
    }

    private void commitInsert() {
        List<Object> insertObjects = context.get(INSERT);
        while (!insertObjects.isEmpty()) {
            Object insertObject = insertObjects.removeLast();
            switch (insertObject) {
                case Courier c -> courierRepository.add(c);
                case Order o -> orderRepository.add(o);
                case OutboxMessage om -> outboxMessageRepository.add(om);
                default -> log.info("Unexpected type on INSERT operation: {}", insertObject.getClass().getName());
            }
        }
    }


    private void commitModify() {
        List<Object> modifyObjects = context.get(MODIFY);
        while (!modifyObjects.isEmpty()) {
            Object modifyObject = modifyObjects.removeLast();
            switch (modifyObject) {
                case Courier c -> courierRepository.update(c);
                case Order o -> orderRepository.update(o);
                case OutboxMessage om -> outboxMessageRepository.add(om);
                default -> log.info("Unexpected type on MODIFY operation: {}", modifyObject.getClass().getName());
            }
        }
    }

    private void commitDelete() {
        context.get(DELETE).forEach(aggregate -> {
            // not implemented;
        });
    }

    private void registerEvents(Aggregate aggregate, UnitOfWorkOperation operation) {
        aggregate.getEvents().forEach(event -> {
            OutboxMessage outboxMessage = OutboxMessage.builder()
                    .id(event.getId())
                    .type(event.getClass().getName())
                    .message(toJson(event))
                    .status(WAITING)
                    .build();
            context.get(operation).add(outboxMessage);
        });
    }
}
