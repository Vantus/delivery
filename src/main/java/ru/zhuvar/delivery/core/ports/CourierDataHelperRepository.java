package ru.zhuvar.delivery.core.ports;

import ru.zhuvar.delivery.core.application.queries.getreadyorbusycouriers.ReadyOrBusyCourier;

import java.util.List;

public interface CourierDataHelperRepository {
    List<ReadyOrBusyCourier> getAllReadyAndBusyCouriers();
}
