package ru.zhuvar.delivery.core.ports;

import ru.zhuvar.delivery.core.domain.order.domainEvents.OrderStatusChangedDomainEvent;

public interface NotificationEventProducer {
    void publishOrderStatusChangedDomainEvent(OrderStatusChangedDomainEvent event);
}
