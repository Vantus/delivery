package ru.zhuvar.delivery.core.ports;

import ru.zhuvar.delivery.utils.extension.outbox.OutboxMessage;

import java.util.List;

public interface OutboxMessageRepository {
    OutboxMessage add(OutboxMessage outboxMessage);

    List<OutboxMessage> getUnposted(int n);

    List<OutboxMessage> updateAll(List<OutboxMessage> outboxMessages);
}
