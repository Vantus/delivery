package ru.zhuvar.delivery.core.ports;

import ru.zhuvar.delivery.core.application.queries.getcreatedorassignedorders.CreatedOrAssignedOrder;

import java.util.List;

public interface OrderDataHelperRepository {
    List<CreatedOrAssignedOrder> getAllCreatedAndAssignedOrders();
}
