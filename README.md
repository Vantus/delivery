# Delivery microservice for DDD + Clean Architecture course

### Run env (without delivery-service inside) for delivery app (env:PG+ADMIN):

```shell
docker-compose -f ./env/local/docker-compose.yml up
```

### Run delivery-app in docker-compose.yml (env:PG+ADMIN):

```shell
docker-compose -f ./docker-compose.yml up
```

### Run app with app dependencies:

```shell
docker-compose -f ./env/app/docker-compose.yml up
```

### Run app env (without delivery-service inside) with app dependencies (NOW NOT WORKING, MB LATER):

```shell
docker-compose -f ./env/app_env/docker-compose.yml up
```

## Hexagonal Architecture Sample

![img.png](img/img.png)

## Command Pattern

![img_1.png](img/img_1.png)

## CQS Showcase

![img_2.png](img/img_2.png)

## Domain Event

![img.png](img/img_4.png)